<?php
require_once(dirname(__FILE__) . '../../../config.php');
require_once('locallib.php');


require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Attempt.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Attempts.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Base.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Grades.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Helper.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Phrase.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Term.php');
require_once($CFG->dirroot . '/mod/phraseanalyzer/classes/Terms.php');
