<?php

require_once('config.php');

// CHECK And PREPARE DATA
global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;
$COURSE;

require_login(1, FALSE);







//--------------------------------------------------------------------------
echo $OUTPUT->header();

$sql = " SELECT"
        . "   mdl_phraseanalyzer.id,"
        . "   mdl_phraseanalyzer.name,"
        . "   mdl_phraseanalyzer.course,"
        . "   mdl_course_modules.course AS course1,"
        . "   mdl_phraseanalyzer_phrase.questiontext,"
        . "   mdl_phraseanalyzer_phrase.phrase,"
        . "   mdl_course_modules.id AS cmid"
        . " FROM"
        . "   mdl_phraseanalyzer"
        . "   INNER JOIN mdl_course_modules ON mdl_course_modules.instance = mdl_phraseanalyzer.id AND mdl_course_modules.course ="
        . "     mdl_phraseanalyzer.course"
        . "   INNER JOIN mdl_phraseanalyzer_phrase ON mdl_phraseanalyzer_phrase.cmid = mdl_course_modules.id"
        . " WHERE"
        . "   mdl_course_modules.module = 36"
        . " AND mdl_phraseanalyzer.id > 9";

$modulesWithPhrases = $DB->get_records_sql($sql);

//print_object($modulesWithPhrases);

foreach ($modulesWithPhrases as $mwp) {
    echo 'id: ' . $mwp->id . ' cmid: ' . $mwp->cmid . ' name: ' . $mwp->name . '<br>';

    $phrase = $DB->get_record('phraseanalyzer_phrase', array('cmid' => $mwp->cmid));
    $terms = $DB->get_records('phraseanalyzer_term', array('cmid' => $mwp->cmid));

    $sql = 'SELECT * FROM {phraseanalyzer} WHERE name = "' . $mwp->name . '" AND id != ' . $mwp->id;
    $updateableModsSql = " SELECT"
            . "   mdl_phraseanalyzer.id,"
            . "   mdl_phraseanalyzer.name,"
            . "   mdl_course_modules.course AS course1,"
            . "   mdl_course_modules.id AS cmid,"
            . "   mdl_phraseanalyzer.course"
            . " FROM"
            . "   mdl_phraseanalyzer"
            . "   INNER JOIN mdl_course_modules ON mdl_course_modules.instance = mdl_phraseanalyzer.id"
            . " WHERE"
            . "   mdl_phraseanalyzer.name = '$mwp->name' AND"
            . "   mdl_course_modules.module = 36 AND"
            . "   mdl_phraseanalyzer.id != $mwp->id";

    $updateableMods = $DB->get_records_sql($updateableModsSql);


    foreach ($updateableMods as $um) {
        print_object('Phrase data');
        $phraseData = array(
            'phraseanalyzerid' => $um->id,
            'cmid' => $um->cmid,
            'questionteext' => $phrase->questiontext,
            'phrase' => $phrase->phrase,
            'column1name' => $phrase->column1name,
            'column2name' => $phrase->column2name,
            'column3name' => $phrase->column3name,
            'column4name' => $phrase->column4name,
            'usecolumn1options' => $phrase->usecolumn1options,
            'usecolumn2options' => $phrase->usecolumn2options,
            'usecolumn3options' => $phrase->usecolumn3options,
            'usecolumn4options' => $phrase->usecolumn4options,
            'column1options' => $phrase->column1options,
            'column2options' => $phrase->column2options,
            'column3options' => $phrase->column3options,
            'column4options' => $phrase->column4options,
            'timecreated' => time(),
            'timemodified' => time()
        );

        if ($DB->insert_record('phraseanalyzer_phrase', $phraseData)) {
            echo 'Phrase entered for ' . $um->name . ' (' . $um->id . ')<br>';
        } else {
            echo '<span style="color: red;">Phrase was not entered for ' . $um->name . ' (' . $um->id . ')</span><br>';
        }


        $x = 0;
        foreach ($terms as $t) {
            print_object('Term data ' . $x);
            $termData = array(
                'phraseanalyzerid' => $um->id,
                'cmid' => $um->cmid,
                'termphrase' => $t->termphrase,
                'answer1' => $t->answer1,
                'answer2' => $t->answer2,
                'answer3' => $t->answer3,
                'answer4' => $t->answer4,
                'timecreated' => time(),
                'timemodified' => time()
            );
            $x++;
            if ($DB->insert_record('phraseanalyzer_term', $termData)) {
                echo 'Term entered for ' . $um->name . ' (' . $um->id . ')<br>';
            } else {
                echo '<span style="color: red;">Term was not entered for ' . $um->name . ' (' . $um->id . ')</span><br>';
            }
        }
    }
}

//**********************
//*** DISPLAY HEADER ***
//**********************
//*** DISPLAY FOOTER ***
//**********************
echo $OUTPUT->footer();
?>
