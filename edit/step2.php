<?php
require_once('../config.php');
// CHECK And PREPARE DATA
global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER, $COURSE;

require_login(1, FALSE);
$phraseid = optional_param('phraseid', 0, PARAM_INT);
$cmid = required_param('cmid', PARAM_INT);



$BASE = new \mod_phraseanalyzer\Base($cmid);

//Set principal parameters
$context = context_module::instance($cmid);

if (!has_capability('mod/phraseanalyzer:addinstance', $context)) {
    redirect($CFG->wwwroot . '/course/view.php?id=' . $COURSE->id);
}
echo $BASE->page($CFG->wwwroot . '/mod/phraseanalyzer/edit/step2.php?cmid=' . $cmid . '&phraseid=' . $phraseid, get_string('editing', 'phraseanalyzer'), get_string('editing', 'phraseanalyzer'), $context);

$PHRASE = new \mod_phraseanalyzer\Phrase($cmid);
$TERMS = new \mod_phraseanalyzer\Terms($cmid);


//--------------------------------------------------------------------------
echo $OUTPUT->header();
//**********************
//*** DISPLAY HEADER ***
$initjs = "$(document).ready(function() {
                        init_phraseAnswers($cmid);
                    });";
echo html_writer::script($initjs);
?>
<div class="container">
    <div class="span12 col-md-12">
        <div class="alert alert-warning">
            <h4><?php echo get_string('the_question', 'phraseanalyzer'); ?></h4>
            <?php echo $PHRASE->getQuestionText(); ?>
        </div>

        <div class="alert alert-default">
            <h4><?php echo get_string('the_phrase', 'phraseanalyzer'); echo $OUTPUT->help_icon('the_phrase', 'phraseanalyzer');?></h4>
            <div id="thePhrase" style="font-size: 18px;" onMouseUp="createTerm(<?php echo $cmid;?>, 'false')"><?php echo $PHRASE->getPhrase(); ?></div>
        </div>

        <div class="alert alert-default">
            <h4><?php echo get_string('the_answers', 'phraseanalyzer'); echo $OUTPUT->help_icon('the_answers', 'phraseanalyzer');?></h4>
            <!--<a href="javascript:void(0)" class="btn btn-success" onClick="createTerm(<?php echo $cmid;?>, 'true')" title="<?php echo get_string('add_term', 'phraseanalyzer');?>" id="addTerm"><?php echo get_string('add_term', 'phraseanalyzer');?></a>-->
            <div class="alert alert-success" id="termSaved"><?php echo get_string('data_saved', 'phraseanalyzer'); ?></div>
            <input type="hidden" id="termCount" value="<?php echo $TERMS->getTermCount();?>">
            <input type="hidden" id="numberOfColumns" value="<?php echo $PHRASE->getNumberOfColumns();?>">
            <table class="table" id="termAnswerTable">
                <thead>
                    <tr>
                        <th>
                            <?php echo $PHRASE->getColumn1Name(); ?>
                        </th>
                        <th>
                            <?php echo $PHRASE->getColumn2Name(); ?>
                        </th>
                        <?php if ($PHRASE->getColumn3Name() != '') { ?>
                            <th>
                                <?php echo $PHRASE->getColumn3Name(); ?>
                            </th>
                        <?php } ?>
                        <?php if ($PHRASE->getColumn4Name() != '') { ?>
                            <th>
                                <?php echo $PHRASE->getColumn4Name(); ?>
                            </th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $TERMS->getTermRows(); ?>
                </tbody>
            </table>
        </div>
        <button id="paSaveAnswers" class="btn btn-primary"><?php echo get_string('save_answers', 'phraseanalyzer'); ?></button>
    </div>
</div>
<?php
//**********************
//*** DISPLAY FOOTER ***
//**********************
echo $OUTPUT->footer();
?>
