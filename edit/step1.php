<?php

require_once('../config.php');
include("step1_form.php");

// CHECK And PREPARE DATA
global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;
$COURSE;

require_login(1, FALSE);
$id = optional_param('id', 0, PARAM_INT);
$cmid = required_param('cmid', PARAM_INT);



$BASE = new \mod_phraseanalyzer\Base($cmid);

//Set principal parameters
$context = context_module::instance($cmid);

if ($id) {
    $formdata = $BASE->getPhrase();

    //Loading Question text into editor
    $draftidQuestionTextEditor = file_get_submitted_draft_itemid('questiontext_edit');
    $currentQuestionText = file_prepare_draft_area($draftidQuestionTextEditor, $context->id, 'mod_phraseanalyzer', 'questiontext', $id, $BASE->getEditorOptions($context), $formdata->questiontext);
    $formdata->questiontext_edit = array('text' => $currentQuestionText, 'format' => FORMAT_HTML, 'itemid' => $draftidQuestionTextEditor);
    $formdata->cmid = $cmid;
    //Loading Phrase text into editor
    $draftidPhraseEditor = file_get_submitted_draft_itemid('phrase_edit');
    $currentPhraseText = file_prepare_draft_area($draftidPhraseEditor, $context->id, 'mod_phraseanalyzer', 'phrase', $id, $BASE->getEditorOptions($context), $formdata->phrase);
    $formdata->phrase_edit = array('text' => $currentPhraseText, 'format' => FORMAT_HTML, 'itemid' => $draftidPhraseEditor);
} else {
    $formdata = new stdClass();
    $formdata->cmid = $cmid;
    $formdata->phraseanalyzerid = $BASE->getPhraseAnalyzerId();
}

echo $BASE->page($CFG->wwwroot . '/mod/phraseanalyzer/edit/step1.php?cmid=' . $cmid . '&id=' . $id, get_string('editing', 'phraseanalyzer'), get_string('editing', 'phraseanalyzer'), $context);

$mform = new step1_form(null, array('formdata' => $formdata));

// If data submitted, then process and store.
if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot . '/mod/phraseanalyzer/view.php?id=' . $cmid);
} else if ($data = $mform->get_data()) {

    if (!isset($data->column1options)) {
        $data->column1options = '';
    }
    if (!isset($data->column2options)) {
        $data->column2options = '';
    }
    if (!isset($data->column3options)) {
        $data->column3options = '';
    }
    if (!isset($data->column4options)) {
        $data->column4options = '';
    }

    if ($data->id) {
        $data->timemodified = time();
        //update record
        $DB->update_record('phraseanalyzer_phrase', $data);
        $recordId = $data->id;
    } else {
        $data->timecreated = time();
        $data->timemodified = time();

        $recordId = $DB->insert_record('phraseanalyzer_phrase', $data);
    }

    $updateData = array();
    $updateData['id'] = $recordId;
    
    //Saving editor text and files
    $draftidQuestionTextEditor = file_get_submitted_draft_itemid('questiontext_edit');
    $questionText = file_save_draft_area_files($draftidQuestionTextEditor, $context->id, 'mod_phraseanalyzer', 'questiontext', $recordId, $BASE->getEditorOptions($context), $data->questiontext_edit['text']);
    $updateData['questiontext'] = $questionText;

    //Saving editor text and files
    $draftidPhraseTextEditor = file_get_submitted_draft_itemid('phrase_edit');
    $phraseText = file_save_draft_area_files($draftidPhraseTextEditor, $context->id, 'mod_phraseanalyzer', 'phrase', $recordId, $BASE->getEditorOptions($context), $data->phrase_edit['text']);
    $updateData['phrase'] = $phraseText;

    $DB->update_record('phraseanalyzer_phrase', $updateData);
    
    redirect($CFG->wwwroot . '/mod/phraseanalyzer/view.php?id=' . $cmid);
}

//--------------------------------------------------------------------------
echo $OUTPUT->header();
//**********************
//*** DISPLAY HEADER ***
$initjs = "$(document).ready(function() {
                        init_edit();
                    });";
echo html_writer::script($initjs);

$mform->display();
//**********************
//*** DISPLAY FOOTER ***
//**********************
echo $OUTPUT->footer();
?>
