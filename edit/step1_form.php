<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main location configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    PLUGINNAME
 * @copyright  2013 Oohoo IT Services Inc.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->dirroot/lib/formslib.php");

/**
 * Module instance settings form
 */
class step1_form extends moodleform {

    function definition() {

        global $CFG, $USER, $DB;
        $formdata = $this->_customdata['formdata'];
        $mform = & $this->_form;
        
        $BASE = new \mod_phraseanalyzer\Base($formdata->cmid);
        $context = context_module::instance($formdata->cmid);

//-------------------------------------------------------------------------------
// Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('phrase', 'phraseanalyzer'));
        $mform->addElement("hidden", "id");
        $mform->setType("id", PARAM_INT);
        $mform->addElement("hidden", "cmid");
        $mform->setType("cmid", PARAM_INT);
        $mform->addElement("hidden", "phraseanalyzerid");
        $mform->setType("phraseanalyzerid", PARAM_INT);
        $mform->addElement("editor", "questiontext_edit", get_string("questiontext", "phraseanalyzer"), null, $BASE->getEditorOptions($context));
        $mform->setType("questiontext_edit", PARAM_RAW);
        $mform->addRule("questiontext_edit", get_string("required", "phraseanalyzer"), "required");
        $mform->addElement("editor", "phrase_edit", get_string("phrase", "phraseanalyzer"), null, $BASE->getEditorOptions($context));
        $mform->setType("phrase_edit", PARAM_RAW);
        $mform->addRule("phrase_edit", get_string("required", "phraseanalyzer"), "required");
        //Column 1
        $mform->addElement('header', 'column1', get_string('column1', 'phraseanalyzer'));
        $mform->addElement("text", "column1name", get_string("name", "phraseanalyzer"));
        $mform->setType("column1name", PARAM_TEXT);        
        //Column 2
        $mform->addElement('header', 'column2', get_string('column2', 'phraseanalyzer'));
        $mform->addElement("text", "column2name", get_string("name", "phraseanalyzer"));
        $mform->setType("column2name", PARAM_TEXT);
        $mform->addElement('selectyesno', 'usecolumn2options', get_string('use_column_options', 'phraseanalyzer'), 0);
        $mform->addHelpButton('usecolumn2options', 'use_column_options', 'phraseanalyzer');
        $mform->setType('usecolumn1options', PARAM_INT);
        $mform->addElement("textarea", "column2options", get_string("column_options", "phraseanalyzer"));
        $mform->setType("column2options", PARAM_TEXT);
        
        //Column 3
        $mform->addElement('header', 'column3', get_string('column3', 'phraseanalyzer'));
        $mform->addElement("text", "column3name", get_string("name", "phraseanalyzer"));
        $mform->setType("column3name", PARAM_TEXT);
        $mform->addElement('selectyesno', 'usecolumn3options', get_string('use_column_options', 'phraseanalyzer'), 0);
        $mform->addHelpButton('usecolumn3options', 'use_column_options', 'phraseanalyzer');
        $mform->setType('usecolumn1options', PARAM_INT);
        $mform->addElement("textarea", "column3options", get_string("column_options", "phraseanalyzer"));
        $mform->setType("column3options", PARAM_TEXT);
        
        //Column 4
        $mform->addElement('header', 'column4', get_string('column4', 'phraseanalyzer'));
        $mform->addElement("text", "column4name", get_string("name", "phraseanalyzer"));
        $mform->setType("column4name", PARAM_TEXT);
        $mform->addElement('selectyesno', 'usecolumn4options', get_string('use_column_options', 'phraseanalyzer'), 0);
        $mform->addHelpButton('usecolumn3options', 'use_column_options', 'phraseanalyzer');
        $mform->setType('usecolumn1options', PARAM_INT);
        $mform->addElement("textarea", "column4options", get_string("column_options", "phraseanalyzer"));
        $mform->setType("column3options", PARAM_TEXT);
        

        $mform->disabledIf("column1options", "usecolumn1options", 'eq', 0);
        $mform->disabledIf("column2options", "usecolumn2options", 'eq', 0);
        $mform->disabledIf("column3options", "usecolumn3options", 'eq', 0);
        $mform->disabledIf("column4options", "usecolumn4options", 'eq', 0);

//-------------------------------------------------------------------------------
// add standard buttons, common to all modules
        $this->add_action_buttons();

// set the defaults
        $this->set_data($formdata);
    }

}
