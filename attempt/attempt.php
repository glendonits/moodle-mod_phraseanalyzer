<?php
/**
 * Parameters are taken directly form view.php
 */
//Set principal parameters
$context = context_module::instance($cm->id);

$PHRASE = new \mod_phraseanalyzer\Phrase($cm->id, $cm->instance);
$TERMS = new \mod_phraseanalyzer\Terms($cm->id);
$ATTEMPT = new \mod_phraseanalyzer\Attempt($cm->id);
//--------------------------------------------------------------------------
?>
<div class="container">
    <div class="span12 col-md-12">
        <div class="alert alert-warning">
            <h4><i><?php echo get_string('instructions', 'phraseanalyzer'); ?></i></h4>
            <?php echo $PHRASE->getQuestionText(); ?>
        </div>

        <div class="alert alert-default">
            <h4><i><?php echo get_string('the_phrase', 'phraseanalyzer');
            echo $OUTPUT->help_icon('the_phrase', 'phraseanalyzer'); ?></i></h4>
            <?php
            if ($ATTEMPT->isAttemptCompleted() == true) {
                ?>
                <span id="thePhrase" style="font-size: 18px;"><?php echo $PHRASE->getPhrase(); ?></span>
                <?php
            } else {
                ?>
                <span id="thePhrase" style="font-size: 18px;" onMouseUp="asCreateTerm(<?php echo $cm->id; ?>)"><?php echo $PHRASE->getPhrase(); ?></span>
                <?php
            }
            ?>
        </div>

        <div class="alert alert-default">
            <h4><?php echo get_string('the_answers', 'phraseanalyzer');
            echo $OUTPUT->help_icon('the_answers', 'phraseanalyzer'); ?></h4>
            <div class="alert alert-success" id="termSaved"><?php echo get_string('data_saved', 'phraseanalyzer'); ?></div>
            <input type="hidden" id="termCount" value="<?php echo $ATTEMPT->getTermCount(); ?>">
            <input type="hidden" id="numberOfColumns" value="<?php echo $PHRASE->getNumberOfColumns(); ?>">
            <table class="table" id="asTermAnswerTable">
                <thead>
                    <tr>
                        <th>
<?php echo $PHRASE->getColumn1Name(); ?>
                        </th>
                        <th>
                        <?php echo $PHRASE->getColumn2Name(); ?>
                        </th>
                            <?php if ($PHRASE->getColumn3Name() != '') { ?>
                            <th>
                            <?php echo $PHRASE->getColumn3Name(); ?>
                            </th>
                        <?php } ?>
                            <?php if ($PHRASE->getColumn4Name() != '') { ?>
                            <th>
                            <?php echo $PHRASE->getColumn4Name(); ?>
                            </th>
<?php } ?>
                    </tr>
                </thead>
                <tbody>
<?php echo $ATTEMPT->getTermRows(); ?>
                </tbody>
            </table>
        </div>
        <?php if ($ATTEMPT->isAttemptCompleted() == true) { ?>
<?php } else { ?>
            <button id="paSaveAnswers" class="btn btn-primary"><?php echo get_string('save_answers', 'phraseanalyzer'); ?></button>
            <a href="javascript:void(0);" id="paSubmitAnswers" class="btn btn-success"><?php echo get_string('submit_answers', 'phraseanalyzer'); ?></a>
<?php } ?>
    </div>
</div>
<div class="simpleModal" data-popup="deleteDialog">
    <div class="simpleModal-inner simpleModal-sm">
        <a class="simpleModal-close" data-popup-close="deleteDialog" href="#">x</a>
        <div class="simpleModal-title"><?php echo get_string('delete_attempt', 'phraseanalyzer');?></div>
        <div class="simpleModal-body">
            <p><?php echo get_string('delete_confirmation', 'phraseanalyzer');?></p>
        </div>
        <div class="simpleModal-footer">
            <a data-popup-close="deleteDialog" href="javascript:void(0);" id="btnCloseDialog" class="btn btn-default" style="float:right;">Close</a>
            <a data-popup-close="deleteDialog" href="javascript:void(0);" id="btnDelete" class="btn btn-primary" style="float:right; margin-bottom:15px; margin-right:10px"><?php echo get_string('delete_attempt', 'phraseanalyzer');?></a>
        </div>
    </div>
</div>
<div id="submitDialog"><?php echo get_string('submit_answers_dialog', 'phraseanalyzer') ?></div>

