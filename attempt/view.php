<?php
require_once( '../config.php');
require_once( '../lib.php');

$id = required_param('id', PARAM_INT); // Course_module ID, or
$userId = required_param('userid', PARAM_INT);  // ... phraseanalyzer instance ID - it should be named as the first character of the module.

if ($id) {
    $cm = get_coursemodule_from_id('phraseanalyzer', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $phraseanalyzer = $DB->get_record('phraseanalyzer', array('id' => $cm->instance), '*', MUST_EXIST);
} else {
    error('You must specify a course_module ID');
}

require_login($course, true, $cm);

$event = \mod_phraseanalyzer\event\attempt_viewed::create(array(
            'objectid' => $PAGE->cm->instance,
            'context' => $PAGE->context,
        ));

$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $phraseanalyzer);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/phraseanalyzer/attempt/view.php', array('id' => $cm->id, 'userid' => $userId));
$PAGE->set_title(format_string($phraseanalyzer->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->jquery_plugin('attemptview', 'mod_phraseanalyzer');

//Load strings for js
$stringman = get_string_manager();
$strings = $stringman->load_component_strings('phraseanalyzer', current_language());
$PAGE->requires->strings_for_js(array_keys($strings), 'phraseanalyzer');

// Output starts here.
echo $OUTPUT->header();

//Set principal parameters
$context = context_module::instance($cm->id);

$PHRASE = new \mod_phraseanalyzer\Phrase($cm->id);
$TERMS = new \mod_phraseanalyzer\Terms($cm->id);
$ATTEMPT = new \mod_phraseanalyzer\Attempt($cm->id, $userId);
$GRADE = new \mod_phraseanalyzer\Grades($context, $cm, $course);
$participants = $GRADE->list_participants(0, false);
//--------------------------------------------------------------------------
?>
<div class="container">
    <input type="hidden" id="cmid" value="<?php echo $id?>">
    <div class="span12 col-md-12">
        <span class="userName"><?php echo fullname($ATTEMPT->getUserDetails()) ?></span>
        <span class="pull-right">
            
            <span class="pull-left"><b><?php echo get_string('participants');?></b></span>  
            <select class="form-control" id="changeUser" mame="changeUser">
                <?php
                foreach ($participants as $p) {
                    if ($userId == $p->id) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }

                    echo '<option value="' . $p->id . '" ' . $selected . '>' . fullname($p) . '</option>';
                }
                ?>
            </select>
        </span>
    </div>
    <div class="span12 col-md-12">

        <div class="alert alert-warning">
            <h4><i><?php echo get_string('instructions', 'phraseanalyzer'); ?></i></h4>
            <?php echo $PHRASE->getQuestionText(); ?>
        </div>

        <div class="alert alert-default">
            <h4><i><?php
                    echo get_string('the_phrase', 'phraseanalyzer');
                    echo $OUTPUT->help_icon('the_phrase', 'phraseanalyzer');
                    ?></i></h4>
            <span id="thePhrase" style="font-size: 18px;"><?php echo $PHRASE->getPhrase(); ?></span>

        </div>

        <div class="alert alert-default">
            <h4><?php
                echo get_string('the_answers', 'phraseanalyzer');
                echo $OUTPUT->help_icon('the_answers', 'phraseanalyzer');
                ?></h4>
            <input type="hidden" id="termCount" value="<?php echo $ATTEMPT->getTermCount(); ?>">
            <input type="hidden" id="numberOfColumns" value="<?php echo $PHRASE->getNumberOfColumns(); ?>">
            <table class="table" id="asTermAnswerTable">
                <thead>
                    <tr>
                        <th>
                            <?php echo $PHRASE->getColumn1Name(); ?>
                        </th>
                        <th>
                            <?php echo $PHRASE->getColumn2Name(); ?>
                        </th>
                        <?php if ($PHRASE->getColumn3Name() != '') { ?>
                            <th>
                                <?php echo $PHRASE->getColumn3Name(); ?>
                            </th>
                        <?php } ?>
                        <?php if ($PHRASE->getColumn4Name() != '') { ?>
                            <th>
                                <?php echo $PHRASE->getColumn4Name(); ?>
                            </th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $ATTEMPT->getTermRows(); ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <a href="<?php echo $CFG->wwwroot;?>/mod/phraseanalyzer/view.php?id=<?php echo $id;?>" class="btn btn-primary"><?php echo get_string('back')?></a>
        
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>