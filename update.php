<?php

require_once('config.php');

// CHECK And PREPARE DATA
global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;
$COURSE;

require_login(1, FALSE);

$sql = "SELECT "
        . "  cm.id, "
        . "  cm.instance "
        . "FROM "
        . "  {modules} m "
        . "  INNER JOIN {course_modules} cm ON cm.module = m.id "
        . "WHERE "
        . "  m.name = 'phraseanalyzer' ";

$modules = $DB->get_records_sql($sql);



foreach ($modules as $cm) {
    $terms = $DB->get_records('phraseanalyzer_term', array('cmid' => $cm->id));

    foreach ($terms as $t) {
        if ($t->phraseanalyzerid == 0) {
            $data = array(
                'id' => $t->id,
                'phraseanalyzerid' => $cm->instance
            );

            $DB->update_record('phraseanalyzer_term', $data);
        }
    }
    $phrases = $DB->get_records('phraseanalyzer_phrase', array('cmid' => $cm->id));
    foreach ($phrases as $p) {
        if ($p->phraseanalyzerid == 0) {
            $pData = array(
                'id' => $p->id,
                'phraseanalyzerid' => $cm->instance
            );
            $DB->update_record('phraseanalyzer_phrase', $pData);
        }
    }
    $attemtps = $DB->get_records('phraseanalyzer_attempt', array('cmid' => $cm->id));
    foreach ($attemtps as $a) {
        if ($a->phraseanalyzerid == 0) {
            $aData = array(
                'id' => $a->id,
                'phraseanalyzerid' => $cm->instance
            );
            $DB->update_record('phraseanalyzer_attempt', $aData);
        }
    }
}

//--------------------------------------------------------------------------
echo $OUTPUT->header();
//**********************
//*** DISPLAY HEADER ***
//**********************
//*** DISPLAY FOOTER ***
//**********************
echo $OUTPUT->footer();
?>
