<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the backup steps that will be used by the backup_phraseanalyzer_activity_task
 *
 * @package   mod_phraseanalyzer
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

/**
 * Define the complete phraseanalyzer structure for backup, with file and id annotations
 *
 * @package   mod_phraseanalyzer
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_phraseanalyzer_activity_structure_step extends backup_activity_structure_step {

    /**
     * Defines the backup structure of the module
     *
     * @return backup_nested_element
     */
    protected function define_structure() {

        // Get know if we are including userinfo.
        $userinfo = $this->get_setting_value('userinfo');

        // Define the root element describing the phraseanalyzer instance.
        $phraseanalyzer = new backup_nested_element('phraseanalyzer', array('id'), array(
            'name', 'intro', 'introformat', 'fromdate', 'enddate', 'grade', 'timecreated', 'timemodified'));

        $phraseanalyzer_phrases = new backup_nested_element('phraseanalyzer_phrases');

        $phraseanalyzer_phrase = new backup_nested_element('phraseanalyzer_phrase', array('id'), array('questiontext',
            'phrase', 'column1name', 'column2name', 'column3name', 'column4name',
            'usecolumn1options', 'column1options', 'usecolumn2options', 'column2options', 'usecolumn3options', 'column3options', 'usecolumn4options', 'column4options',
            'timecreated', 'timemodified'));

        $phraseanalyzer_terms = new backup_nested_element('phraseanalyzer_terms');

        $phraseanalyzer_term = new backup_nested_element('phraseanalyzer_term', array('id'), array('termphrase', 'termphraseoffset',
            'answer1', 'answer2', 'answer3', 'answer4', 'timecreated', 'timemodified'));


        $phraseanalyzer_attempts = new backup_nested_element('phraseanalyzer_attempts');

        $phraseanalyzer_attempt = new backup_nested_element('phraseanalyzer_attempt', array('id'), array('userid',
            'answer1', 'answers', 'userrole', 'attempt', 'timecreated', 'timemodified', 'completed'));

        $phraseanalyzer->add_child($phraseanalyzer_phrases);
        $phraseanalyzer->add_child($phraseanalyzer_terms);
        $phraseanalyzer->add_child($phraseanalyzer_attempts);
        $phraseanalyzer_phrases->add_child($phraseanalyzer_phrase);
        $phraseanalyzer_terms->add_child($phraseanalyzer_term);
        $phraseanalyzer_attempts->add_child($phraseanalyzer_attempt);

        // If we had more elements, we would build the tree here.
        // Define data sources.
        $phraseanalyzer->set_source_table('phraseanalyzer', array('id' => backup::VAR_ACTIVITYID));
        
        $phraseanalyzer_phrase->set_source_sql(
                'SELECT * '
                . 'FROM {phraseanalyzer_phrase} '
                . 'WHERE phraseanalyzerid = ?', array(backup::VAR_ACTIVITYID));
        
        $phraseanalyzer_term->set_source_sql(
                'SELECT * '
                . 'FROM {phraseanalyzer_term} '
                . 'WHERE phraseanalyzerid = ?', array(backup::VAR_ACTIVITYID));
        
        if ($userinfo) {
            $phraseanalyzer_attempt->set_source_sql(
                    'SELECT * '
                    . 'FROM {phraseanalyzer_attempt} '
                    . 'WHERE phraseanalyzerid = ?', array(backup::VAR_ACTIVITYID));
        }

        $phraseanalyzer_attempt->annotate_ids('user', 'userid');

        // If we were referring to other tables, we would annotate the relation
        // with the element's annotate_ids() method.
        // Define file annotations (we do not use itemid in this example).
        $phraseanalyzer->annotate_files('mod_phraseanalyzer', 'intro', null);
        $phraseanalyzer_phrase->annotate_files('mod_phraseanalyzer', 'questiontext', backup::VAR_PARENTID);
        $phraseanalyzer_phrase->annotate_files('mod_phraseanalyzer', 'phrase', backup::VAR_PARENTID);

        // Return the root element (phraseanalyzer), wrapped into standard activity structure.
        return $this->prepare_activity_structure($phraseanalyzer);
    }

    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot, "/");

        // Link to the list of choices
        $search = "/(" . $base . "\/mod\/phraseanalyzer\/index.php\?id\=)([0-9]+)/";
        $content = preg_replace($search, '$@PHRASEANALYZERINDEX*$2@$', $content);

        // Link to choice view by moduleid
        $search = "/(" . $base . "\/mod\/phraseanalyzer\/view.php\?id\=)([0-9]+)/";
        $content = preg_replace($search, '$@PHRASEANALYZERVIEWBYID*$2@$', $content);

        return $content;
    }

}
