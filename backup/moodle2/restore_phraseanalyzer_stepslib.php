<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the restore steps that will be used by the restore_phraseanalyzer_activity_task
 *
 * @package   mod_phraseanalyzer
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Structure step to restore one phraseanalyzer activity
 *
 * @package   mod_phraseanalyzer
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_phraseanalyzer_activity_structure_step extends restore_activity_structure_step {

    /**
     * Defines structure of path elements to be processed during the restore
     *
     * @return array of {@link restore_path_element}
     */
    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('phraseanalyzer', '/activity/phraseanalyzer');
        $paths[] = new restore_path_element('phraseanalyzer_phrase', '/activity/phraseanalyzer/phraseanalyzer_phrases/phraseanalyzer_phrase');
        $paths[] = new restore_path_element('phraseanalyzer_term', '/activity/phraseanalyzer/phraseanalyzer_terms/phraseanalyzer_term');
        if ($userinfo) {
            $paths[] = new restore_path_element('phraseanalyzer_attempt', '/activity/phraseanalyzer/phraseanalyzer_attempts/phraseanalyzer_attempt');
        }

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    /**
     * Process the given restore path element data
     *
     * @param array $data parsed element data
     */
    protected function process_phraseanalyzer($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timecreated = time();
        $data->timemodified = time();


        if ($data->grade < 0) {
            // Scale found, get mapping.
            $data->grade = -($this->get_mappingid('scale', abs($data->grade)));
        }

        // Create the phraseanalyzer instance.
        $newitemid = $DB->insert_record('phraseanalyzer', $data);
        $this->apply_activity_instance($newitemid);
    }

    protected function process_phraseanalyzer_phrase($data) {
        global $DB;

        $data = (object) $data;
        $oldid = $data->id;

        $data->phraseanalyzerid = $this->get_new_parentid('phraseanalyzer');

        $data->timecreated = time();
        $data->timemodified = time();

        $newitemid = $DB->insert_record('phraseanalyzer_phrase', $data);
        $this->set_mapping('phraseanalyzer_phrase', $oldid, $newitemid, true); //has related files
    }
    
    protected function process_phraseanalyzer_term($data) {
        global $DB;

        $data = (object) $data;
        $oldid = $data->id;

        $data->phraseanalyzerid = $this->get_new_parentid('phraseanalyzer');

        $data->timecreated = time();
        $data->timemodified = time();

        $newitemid = $DB->insert_record('phraseanalyzer_term', $data);
        $this->set_mapping('phraseanalyzer_term', $oldid, $newitemid, true); //has related files
    }
    
    protected function process_phraseanalyzer_attempt($data) {
        global $DB;

        $data = (object) $data;
        $oldid = $data->id;

        $data->phraseanalyzerid = $this->get_new_parentid('phraseanalyzer');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('phraseanalyzer_attempt', $data);
    }

    /**
     * Post-execution actions
     */
    protected function after_execute() {
        // Add phraseanalyzer related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_phraseanalyzer', 'intro', null);
        $this->add_related_files('mod_phraseanalyzer', 'questiontext', null);
        $this->add_related_files('mod_phraseanalyzer', 'phrase', null);
    }

}
