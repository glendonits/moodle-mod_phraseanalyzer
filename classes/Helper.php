<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of Helper
 *
 * @author patrick
 */
class Helper {

    /**
     * 
     * @param array $data An array containing terms that will be added to the options of the select menu
     * @param string $id (Required)An element id
     * @param string $name(optional)An element id     
     */
    public static function createSelectMenu($data, $id, $name = null, $selected = '', $style='') {
        //Make name the same as id if non supplied
        if (!isset($name)) {
            $name = $id;
        }

        $html = '<select class="form-control" id="' . $id . '" name="' . $name . '" ' . $style . '>';
        $html .= '  <option value="0">' . get_string('select', 'phraseanalyzer') . '</option>';
        for ($i = 0; $i < count($data); $i++) {
            if (trim($selected) == trim($data[$i])) {
                $html .= '  <option value="' . $data[$i] . '" selected>' . $data[$i] . '</option>';
            } else {
                $html .= '  <option value="' . $data[$i] . '">' . $data[$i] . '</option>';
            }
        }
        $html .= '</select>';

        return $html;
    }

}
