<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of Terms
 *
 * @author patrick
 */
class Term {
    
    private $cmid;
    private $phraseId;
    
    public function __construct($cmid) {
        $this->cmid = $cmid;
        $cm = get_coursemodule_from_id('phraseanalyzer', $this->cmid, 0, false, MUST_EXIST);
        $this->phraseId = $cm->instance;
    }
    
    public function getCmid() {
        return $this->cmid;
    }

    /**
     * 
     * @global \moodle_database $DB
     * @param string $term
     * @param string $answer1
     * @param string $answer2
     * @param string $answer3
     */    
    public function save($term, $termOffset, $answer1, $answer2, $answer3) {
        global $DB;
                
        if ($answer1 == 'undefined') {
            $answer1 = '';
        }
        if ($answer2 == 'undefined') {
            $answer2 = '';
        }
        if ($answer3 == 'undefined') {
            $answer3 = '';
        }
        
        $term = trim($term);
        $term = str_replace('’' , '\'', $term);
        //Look to see if the term already exists
        $sql = 'SELECT * FROM {phraseanalyzer_term} WHERE phraseanalyzerid = ' . $this->phraseId . ' AND termphraseoffset = "' . $termOffset . '"';
        if ($termRecord = $DB->get_record_sql($sql)) {
            $data = new \stdClass();
            $data->id = $termRecord->id;
            $data->answer1 = str_replace('’', '\'', $answer1);
            $data->answer2 = str_replace('’', '\'', $answer2);
            $data->answer3 = str_replace('’', '\'', $answer3);
            $data->timemodified = time();
            $DB->update_record('phraseanalyzer_term', $data);
        } else {
            $data = new \stdClass();
            $data->termphrase = $term;
            $data->termphraseoffset = $termOffset;
            $data->phraseanalyzerid = $this->phraseId;
            $data->answer1 = str_replace('’', '\'', $answer1);
            $data->answer2 = str_replace('’', '\'', $answer2);
            $data->answer3 = str_replace('’', '\'', $answer3);
            $data->timecreated = time();
            $data->timemodified = time();
            $DB->insert_record('phraseanalyzer_term', $data);
        }
    }
    
    public function delete($term) {
        global $DB;
        $sql = 'SELECT * FROM {phraseanalyzer_term} WHERE phraseanalyzerid = ' . $this->phraseId. ' AND termphraseoffset = "' . $term . '"';
        $termRecord = $DB->get_record_sql($sql);
        $DB->delete_records('phraseanalyzer_term', array('id' => $termRecord->id));
    }
}
