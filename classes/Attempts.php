<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of Terms
 *
 * @author patrick
 */
class Attempts {

    private $cmId;
    private $userId;
    private $role;

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \stdClass $USER
     * @param int $phraseId
     * @param int $role
     */
    public function __construct($cmId, $role = Base::ROLE_STUDENT) {
        global $CFG, $DB, $USER;
        $this->cmId = $cmId;
        $this->userId = $USER->id;
        $this->role = $role;
    }

    /**
     * 
     * @global \moodle_database $DB
     */
    public function getTermRows() {
        global $DB;

        $PHRASE = new Phrase($this->cmId);
        $numberOfColumns = $PHRASE->getNumberOfColumns();

        $html = '';
        if ($attempts = $DB->get_record('phraseanalyzer_attempt', array('phraseanalyzerid' => $PHRASE->getId(), 'userid' => $this->userId))) {

            $i = 0;
            foreach ($attempts as $t) {
                $answer1 = $t->column2;
                $answer2 = $t->column3;
                $answer3 = $t->column4;
                $html .= '<tr>';
                $html .= '<td><input type="hidden" value="' . $t->column1 . '" id="term' . $i . '" name="term' . $i . '"/>' . $t->column1 . '<a href="#" class="deleteRow pull-right" title="' . $t->column1 . '" ><i class="fa fa-trash"></i></a></td>';
                $html .= '<td><textarea class="form-control" id="answer1' . $i . '" name="answer1' . $i . '">' . $answer1 . '</textarea></td>';
                if ($numberOfColumns >= 3) {
                    $html .= '<td><textarea class="form-control" id="answer2' . $i . '" name="answer1' . $i . '">' . $answer2 . '</textarea></td>';
                }
                if ($numberOfColumns == 4) {
                    $html .= '<td><textarea class="form-control" id="answer3' . $i . '" name="answer3' . $i . '">' . $answer3 . '</textarea></td>';
                }
                $i++;
            }
        }
        return $html;
    }

}
