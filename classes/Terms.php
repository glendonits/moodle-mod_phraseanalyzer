<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of Terms
 *
 * @author patrick
 */
class Terms extends \mod_phraseanalyzer\Base {

    public $phraseAnalyzerId;
    public $phrase;
    public $cm;
    
    public function __construct($cmid) {
        global $DB;
        
        parent::__construct($cmid);
        
        $this->cmid = $cmid;
        $this->cm = parent::getCm();
        $this->phraseAnalyzerId = $this->cm->instance;
    }

    /**
     * 
     * @global \moodle_database $DB
     */
    public function getTermRows() {
        global $DB;
        
        $PHRASE = new Phrase($this->cmid);
        $numberOfColumns = $PHRASE->getNumberOfColumns();
        
        $terms = parent::getTerms();
        $i = 1;
        $html = '';
        foreach ($terms as $t) {
            $answer1 = str_replace('@@',"\n",$t->answer1);
            $answer2 = str_replace('@@',"\n",$t->answer2);
            $answer3 = str_replace('@@',"\n",$t->answer3);
            
            /**
             * Note that input names and ids are based on column numbers
             */
            $html .= '<tr>';
            $html .= '<td><div class="col-md-11"><input type="hidden" id="termOffset' . $i . '" value="' . $t->termphraseoffset . '"><textarea class="form-control"  id="term' . $i .  '" name="term' . $i .  '">'. $t->termphrase .'</textarea></div>' .  '<div class="col-md-1"><a href="#" class="deleteRow" title="' . $t->termphraseoffset . '" ><i class="fa fa-trash"></i></a></div></td>';
            $html .= '<td><textarea class="form-control" id="answer1-' . $i .  '" name="answer1-' . $i . '">' . $answer1 . '</textarea></td>';
            if ($numberOfColumns >= 3) {
                $html .= '<td><textarea class="form-control" id="answer2-' . $i .  '" name="answer2-' . $i . '">' . $answer2 . '</textarea></td>';
            }
            if ($numberOfColumns == 4) {
                $html .= '<td><textarea class="form-control" id="answer3-' . $i .  '" name="answer3-' . $i . '">' . $answer3 . '</textarea></td>';
            }
            $i++;
        }
        return $html;
        
    }
    
    public function getTermCount() {
        global $DB;
        $terms = parent::getTerms();
        return count($terms);
    }
    
    /**
     * array to compare with for correct answers
     */
    public function getTermsArray() {
        global $DB;
        $terms = parent::getTerms();
        $terms = convert_to_array($terms);
        return $terms;
    }
}
