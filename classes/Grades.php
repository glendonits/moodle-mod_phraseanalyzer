<?php

namespace mod_phraseanalyzer;

defined('MOODLE_INTERNAL') || die();


// Grading states.
define('PHRASEANALYZER_GRADING_STATUS_GRADED', 'graded');
define('PHRASEANALYZER_GRADING_STATUS_NOT_GRADED', 'notgraded');

require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/grade/grading/lib.php');

/**
 *
 * Base class for grading,
 *
 * @package    mod_phraseanalyzer
 * @copyright  2017 Patrick Thibaudeau
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Grades {

    /** @var stdClass the phraseanalyzer record that contains the global settings for this assign instance */
    private $instance;

    /** @var grade_item the grade_item record for this phraseanalyzer instance's primary grade item. */
    private $gradeitem;

    /** @var context the context of the course module for this phraseanalyzer instance
     *               (or just the course if we are creating a new one)
     */
    private $context;

    /** @var stdClass the course this phraseanalyzer instance belongs to */
    private $course;

    /** @var cm_info the course module for this phraseanalyzer instance */
    private $coursemodule;

    /** @var string modulename prevents excessive calls to get_string */
    private static $modulename = null;

    /** @var string modulenameplural prevents excessive calls to get_string */
    private static $modulenameplural = null;

    /** @var array cached list of participants for this phraseanalyzer. The cache key will be group, showactive and the context id */
    private $participants = array();

    /** @var array cached list of user groups are enabled. The cache key will be the user. */
    private $usersubmissiongroups = array();

    /** @var array cached list of user groups. The cache key will be the user. */
    private $usergroups = array();

    /** @var array cached list of IDs of users who share group membership with the user. The cache key will be the user. */
    private $sharedgroupmembers = array();

    /**
     * Constructor for the base phraseanalyzer class.
     *
     * Note: For $coursemodule you can supply a stdclass if you like, but it
     * will be more efficient to supply a cm_info object.
     *
     * @param mixed $coursemodulecontext context|null the course module context
     *                                   (or the course context if the coursemodule has not been
     *                                   created yet).
     * @param mixed $coursemodule the current course module if it was already loaded,
     *                            otherwise this class will load one from the context as required.
     * @param mixed $course the current course  if it was already loaded,
     *                      otherwise this class will load one from the context as required.
     */
    public function __construct($coursemodulecontext, $coursemodule, $course) {
        global $SESSION;

        $this->context = $coursemodulecontext;
        $this->course = $course;

        // Ensure that $this->coursemodule is a cm_info object (or null).
        $this->coursemodule = \cm_info::create($coursemodule);

        // Temporary cache only lives for a single request - used to reduce db lookups.
        $this->cache = array();

        // Extra entropy is required for uniqid() to work on cygwin.
        $this->useridlistid = clean_param(uniqid('', true), PARAM_ALPHANUM);

        if (!isset($SESSION->mod_phraseanalyzer_useridlist)) {
            $SESSION->mod_phraseanalyzer_useridlist = [];
        }
    }

    /**
     * Return the current instance id based on context
     * @return int
     */
    public function get_instance() {
        $context = convert_to_array($this->context);
        return $context['instanceid'];
    }

    public function get_course_module() {
        return $this->coursemodule;
    }

    /**
     * Load a list of users enrolled in the current course with the specified permission and group.
     * 0 for no group.
     *
     * @param int $currentgroup
     * @param bool $idsonly
     * @return array List of user records
     */
    public function list_participants($currentgroup, $idsonly) {
        global $DB, $USER;

        if (empty($currentgroup)) {
            $currentgroup = 0;
        }

        $key = $this->context->id . '-' . $currentgroup;
        if (!isset($this->participants[$key])) {
            list($esql, $params) = get_enrolled_sql($this->context, 'mod/phraseanalyzer:submit', $currentgroup, true);

            $fields = 'u.*';
            $orderby = 'u.lastname, u.firstname, u.id';
            $additionaljoins = '';
            $additionalfilters = '';

            $sql = "SELECT $fields
                      FROM {user} u
                      JOIN ($esql) je ON je.id = u.id
                           $additionaljoins
                     WHERE u.deleted = 0
                           $additionalfilters
                  ORDER BY $orderby";

            $users = $DB->get_records_sql($sql, $params);

            $cm = $this->get_course_module();
            $info = new \core_availability\info_module($cm);
            $users = $info->filter_user_list($users);

            $this->participants[$key] = $users;

            if ($idsonly) {
                $idslist = array();
                foreach ($this->participants[$key] as $id => $user) {
                    $idslist[$id] = new stdClass();
                    $idslist[$id]->id = $id;
                }
                return $idslist;
            }
            return $this->participants[$key];
        }
    }

    /**
     * Prints table with user grade
     * @global \stdClass $CFG
     * @return string
     */
    public function get_front_page_submission_view() {
        global $CFG, $PAGE;
        $participants = $this->list_participants(0, false);

        $html = '<table id="participantSubmission" class="table">';
        $html .= '   <thead>';
        $html .= '       <tr>';
        $html .= '           <th></th>';
        $html .= '           <th>' . get_string('lastname') . '</th>';
        $html .= '           <th>' . get_string('firstname') . '</th>';
        $html .= '           <th>' . get_string('grade', 'phraseanalyzer') . '</th>';
        $html .= '           <th>' . get_string('action', 'phraseanalyzer') . '</th>';
        $html .= '       </tr>';
        $html .= '   </thead>';
        $html .= '   <tbody>';
        foreach ($participants as $p) {
            if (isset($ATTEMPT)) {
                unset($ATTEMPT);
            }

            $cm = $this->coursemodule->get_course_module_record();
            $ATTEMPT = new Attempt($cm->id, $p->id);
            $grade = $ATTEMPT->getGrade();
            //Get user picture
            $userPicture = new \user_picture($p);
            $userPictureSrc = $userPicture->get_url($PAGE);

            $ATTEMPT = new Attempt($cm->id, $p->id);
            $html .= '      <tr>';
            $html .= '          <td>';
            $html .= '              <a href="' . $CFG->wwwroot . '/user/view.php?id=' . $p->id . '&course=' . $cm->course . '"><img src="' . $userPictureSrc . '"></a>';
            $html .= '          </td>';
            $html .= '          <td>';
            $html .= '              ' . $p->lastname;
            $html .= '          </td>';
            $html .= '          <td>';
            $html .= '              ' . $p->firstname;
            $html .= '          </td>';
            $html .= '          <td>';
            $html .= '              ' . $grade->percentage;
            $html .= '          </td>';
            $html .= '          <td>';
            if ($ATTEMPT->isAttemptCompleted()) {
                $html .= '              <a href="' . $CFG->wwwroot . '/mod/phraseanalyzer/attempt/view.php?id=' . $cm->id . '&userid=' . $p->id . '">' . get_string('view_attempt', 'phraseanalyzer') . '</a>';
                $html .= '              <a href="javascript:void(0)" style="margin-left: 10px;" class="btn btn-danger btn-sm" title="' . get_string('delete_attempt', 'phraseanalyzer') . '" onClick="deleteAttempt(' . $cm->id . ', ' . $p->id . ')"><i class="fa fa-trash"></i></a>';
            }
            $html .= '          </td>';
            $html .= '      </tr>';
        }
        $html .= '   </tbody>';
        $html .= '</table>';

        return $html;
    }

}
