<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of phrase
 *
 * @author patrick
 */
class Phrase extends \mod_phraseanalyzer\Base{

    public $id;
    public $cmId;
    public $questionText;
    public $phrase;
    public $column1Name;
    public $column2Name;
    public $column3Name;
    public $column4Name;
    public $useColumn1Options;
    public $useColumn2Options;
    public $useColumn3Options;
    public $useColumn4Options;
    public $column1Options;
    public $column2Options;
    public $column3Options;
    public $column4Options;
    public $timeCreated;
    public $timeCreatedHr;
    public $timeModified;
    public $timeModifiedHr;

    /**
     * 
     * @global \moodle_database $DB
     * @global \stdClass $CFG
     * @param int $id
     */
    public function __construct($cmid) {
        global $DB, $CFG;

        include_once($CFG->dirroot . '/lib/filelib.php');
        
        parent::__construct($cmid);

        $context = \context_module::instance($cmid);
        $phrase = parent::getPhrase();
        
        $this->id = $phrase->id;
        $this->cmId = $cmid;
        

        if (isset($phrase->questiontext)) {
            $questionText = file_rewrite_pluginfile_urls($phrase->questiontext, 'pluginfile.php', $context->id, 'mod_phraseanalyzer', 'questiontext', $phrase->id);
            $questionTextOptions = new \stdClass;
            $questionTextOptions->noclean = true;
            $questionTextOptions->overflowdiv = true;
            $questionTextOptions->context = $context;
            $questionText = format_text($questionText, FORMAT_HTML, $questionTextOptions);
        } else {
            $questionText = '';
        }

        $this->questionText = $questionText;
        if (isset($phrase->phrase)) {
            $phraseText = file_rewrite_pluginfile_urls($phrase->phrase, 'pluginfile.php', $context->id, 'mod_phraseanalyzer', 'phrase', $phrase->id);
            $phraseTextOptions = new \stdClass;
            $phraseTextOptions->noclean = true;
            $phraseTextOptions->overflowdiv = true;
            $phraseTextOptions->context = $context;
            $phraseText = format_text($phraseText, FORMAT_HTML, $phraseTextOptions);
        } else {
            $phraseText = '';
        }
        $this->phrase = $phraseText;
        if (isset($phrase->column1name)) {
            $this->column1Name = $phrase->column1name;
        } else {
            $this->column1Name = '';
        }

        if (isset($phrase->column2name)) {
            $this->column2Name = $phrase->column2name;
        } else {
            $this->column2Name = '';
        }
        if (isset($phrase->column3name)) {
            $this->column3Name = $phrase->column3name;
        } else {
            $this->column3Name = '';
        }
        if (isset($phrase->column4name)) {
            $this->column4Name = $phrase->column4name;
        } else {
            $this->column4Name = '';
        }

        if (isset($phrase->usecolumn1options)) {
            $this->useColumn1Options = $phrase->usecolumn1options;
        } else {
            $this->useColumn1Options = '';
        }

        if (isset($phrase->usecolumn2options)) {
            $this->useColumn2Options = $phrase->usecolumn2options;
        } else {
            $this->useColumn2Options = '';
        }

        if (isset($phrase->usecolumn3options)) {
            $this->useColumn3Options = $phrase->usecolumn3options;
        } else {
            $this->useColumn3Options = '';
        }

        if (isset($phrase->usecolumn4options)) {
            $this->useColumn4Options = $phrase->usecolumn4options;
        } else {
            $this->useColumn4Options = '';
        }

        if (isset($phrase->column1options)) {
            $this->column1Options = $phrase->column1options;
        } else {
            $this->column1Options = '';
        }

        if (isset($phrase->column2options)) {
            $this->column2Options = $phrase->column2options;
        } else {
            $this->column2Options = '';
        }

        if (isset($phrase->column3options)) {
            $this->column3Options = $phrase->column3options;
        } else {
            $this->column3Options = '';
        }

        if (isset($phrase->column4options)) {
            $this->column4Options = $phrase->column4options;
        } else {
            $this->column4Options = '';
        }

        if (isset($phrase->timecreated)) {
            $this->timeCreated = $phrase->timecreated;
            $this->timeCreatedHr = date('d-m-Y', $phrase->timecreated);
        } else {
            $this->timeCreated = '';
            $this->timeCreatedHr = '';
        }

        if (isset($phrase->timemodified)) {
        $this->timeModified = $phrase->timemodified;
        $this->timeModifiedHr = date('d-m-Y', $phrase->timemodified);
        } else {
            $this->timeModified = '';
            $this->timeModifiedHr = '';
        }

    }

    public function getId() {
        return $this->id;
    }

    public function getCmId() {
        return $this->cmId;
    }

    public function getQuestionText() {
        return $this->questionText;
    }

    public function getPhrase() {
        return $this->phrase;
    }

    public function getColumn1Name() {
        return $this->column1Name;
    }

    public function getColumn2Name() {
        return $this->column2Name;
    }

    public function getColumn3Name() {
        return $this->column3Name;
    }

    public function getColumn4Name() {
        return $this->column4Name;
    }

    public function getUseColumn1Options() {
        return $this->useColumn1Options;
    }

    public function getUseColumn2Options() {
        return $this->useColumn2Options;
    }

    public function getUseColumn3Options() {
        return $this->useColumn3Options;
    }

    public function getUseColumn4Options() {
        return $this->useColumn4Options;
    }

    public function getColumn1Options() {
        return $this->column1Options;
    }

    public function getColumn2Options() {
        return $this->column2Options;
    }

    public function getColumn3Options() {
        return $this->column3Options;
    }

    public function getColumn4Options() {
        return $this->column4Options;
    }

    public function getTimeCreated() {
        return $this->timeCreated;
    }

    public function getTimeCreatedHr() {
        return $this->timeCreatedHr;
    }

    public function getTimeModified() {
        return $this->timeModified;
    }

    public function getTimeModifiedHr() {
        return $this->timeModifiedHr;
    }

    /**
     * Returns a select menu for column 1 options
     * @return html
     */
    public function getColumn1OptionsMenu() {
        $data = explode("\n", $this->column1Options);
        return Helper::createSelectMenu($data, 'column1Options');
    }

    /**
     * Returns a select menu for column 2 options
     * @return html
     */
    public function getColumn2OptionsMenu() {
        $data = explode("\n", $this->column2Options);
        return Helper::createSelectMenu($data, 'column2Options');
    }

    /**
     * Returns a select menu for column 3 options
     * @return html
     */
    public function getColumn3OptionsMenu() {
        $data = explode("\n", $this->column3Options);
        return Helper::createSelectMenu($data, 'column3Options');
    }

    /**
     * Returns a select menu for column 4 options
     * @return html
     */
    public function getColumn4OptionsMenu() {
        $data = explode("\n", $this->column4Options);
        return Helper::createSelectMenu($data, 'column4Options');
    }

    public function getNumberOfColumns() {
        $default = 2; //Name and atleast one answer
        
        if ($this->column3Name != '') {
            $default = 3;
        }
        if ($this->column4Name != '') {
            $default = 4;
        }

        return $default;
    }

    /**
     * 
     * @param int $columnNumber
     * @param int $termCount
     * @param string $value
     * @return string
     */
    public function getColumnHtml($columnNumber, $termCount, $value = '', $style = '') {
        $useColumnName = 'useColumn' . $columnNumber . 'Options';
        $columnName = 'column' . $columnNumber . 'Options';
        if ($this->$useColumnName == 1) {
            $data = explode("\n", $this->$columnName);
            $html = Helper::createSelectMenu($data, 'answer' . $columnNumber . '-' . $termCount, 'answer' . $columnNumber . '-' . $termCount, $value, $style);
        } else {
            $html = '<input type="text" class="form-control" id="answer' . $columnNumber . '-' . $termCount . '" name="answer' . $columnNumber . '-' . $termCount . '" value="' . $value . '" ' . $style . '>';
        }

        return $html;
    }

}
