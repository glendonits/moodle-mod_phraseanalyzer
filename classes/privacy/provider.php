<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer\privacy;

use context;
use context_helper;
use stdClass;
use core_privacy\local\metadata\collection;
use \core_privacy\local\metadata\provider as metadataprovider;
use \core_privacy\local\request\plugin\provider as pluginprovider;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\helper;
use core_privacy\local\request\transform;
use core_privacy\local\request\writer;

// This plugin does store personal user data.
class provider implements metadataprovider, pluginprovider {

    public static function get_metadata(collection $collection): collection {

        $phraseanalyzer_attempt = [
            'userid' => 'privacy:metadata:userid',
            'phraseanalyzerid' => 'privacy:metadata:phraseanalyzerid',
            'answers' => 'privacy:metadata:answers',
            'attempt' => 'privacy:metadata:attempt',
            'timecreated' => 'privacy:metadata:timecreated',
            'timemodified' => 'privacy:metadata:timemodified',
            'completed' => 'privacy:metadata:completed',
        ];

        $collection->add_database_table('phraseanalyzer_attempt', $phraseanalyzer_attempt, 'privacy:metadata:phraseanalyzer_attempt');

        return $collection;
    }

    public static function get_contexts_for_userid(int $userid): contextlist {
        global $DB;

        $contextlist = new contextlist();

        $params = [
            'modname' => 'phraseanalyzer',
            'contextlevel' => CONTEXT_MODULE,
            'userid' => $userid
        ];

        $sql = "SELECT c.id
                 FROM {context} c
           INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
           INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
           INNER JOIN {phraseanalyzer} p ON p.id = cm.instance
            LEFT JOIN {phraseanalyzer_attempt} pa ON phraseanalyzerid. = p.id
                WHERE (
                pa.userid        = :userid
                )
        ";

        $contextlist->add_from_sql($sql, $params);

        return $contextlist;
    }

    public static function export_user_data(approved_contextlist $contextlist) {
        foreach ($contextlist->get_contexts() as $context) {
            // Check that the context is a module context.
            if ($context->contextlevel != CONTEXT_MODULE) {
                continue;
            }
            $user = $contextlist->get_user();
            $attemptdata = helper::get_context_data($context, $user);
            helper::export_context_files($context, $user);

            writer::with_context($context)->export_data([], $attemptdata);
        }
    }

    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;

        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }

        $cm = get_coursemodule_from_id('phraseanalyzer', $context->instanceid);
        if (!$cm) {
            return;
        }

        $DB->delete_records('phraseanalyzer_attempt', ['phraseanalyzerid' => $cm->instance]);
    }

    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;

        if (empty($contextlist->count())) {
            return;
        }
        $userid = $contextlist->get_user()->id;
        foreach ($contextlist->get_contexts() as $context) {
            $instanceid = $DB->get_field('course_modules', 'instance', ['id' => $context->instanceid], MUST_EXIST);
            $DB->delete_records('phraseanalyzer_attempt', ['phraseanalyzerid' => $instanceid, 'userid' => $userid]);
        }
    }

}
