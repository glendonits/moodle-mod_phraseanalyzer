<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace mod_phraseanalyzer;

/**
 * Description of Attempt
 *
 * @author patrick
 */
class Attempt {

    public $attemptId;
    public $cmId;
    public $phraseId;
    public $userId;
    public $attempt;
    public $userDetails;
    public $cm;
    private $penalty;

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \stdClass $USER
     * @param int $cmId
     * @param int $userId
     * @param \stdClass $userDetails
     */
    public function __construct($cmId, $userId = 0) {
        global $CFG, $DB, $USER;
        
        $BASE = new Base($cmId);

        $cm = get_coursemodule_from_id('phraseanalyzer', $cmId, 0, false, MUST_EXIST);
        //Set userid based on logged in user if userId  == 0
        if ($userId == 0) {
            $userId = $USER->id;
        }

        if (!$attempt = $DB->get_record('phraseanalyzer_attempt', array('phraseanalyzerid' => $cm->instance, 'userid' => $userId))) {
            $this->attemptId = $DB->insert_record('phraseanalyzer_attempt', array('userid' => $userId, 'phraseanalyzerid' => $cm->instance, 'timecreated' => time()));
        } else {
            $this->attemptId = $attempt->id;
        }
        $this->cmId = $cmId;
        $this->userId = $userId;
        $this->attempt = $DB->get_record('phraseanalyzer_attempt', array('phraseanalyzerid' => $cm->instance, 'userid' => $userId));
        $this->userDetails = $DB->get_record('user', array('id' => $userId));
        $this->cm = get_coursemodule_from_id('phraseanalyzer', $cmId, 0, false, MUST_EXIST);
        $this->phraseId = $this->cm->instance;
        $this->penalty = $BASE->getPenalty();
    }

    public function getCmid() {
        return $this->cmid;
    }

    public function getAttemptId() {
        return $this->attemptId;
    }

    public function getPhraseId() {
        return $this->phraseId;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getAttempt() {
        return $this->attempt;
    }

    public function getUserDetails() {
        return $this->userDetails;
    }

    /**
     * 
     * @global \moodle_database $DB
     * @global \stdClass $USER
     * @param string $answers
     * @param int $submit
     */
    public function save($answers, $submit) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . '/mod/phraseanalyzer/lib.php');
        //empty out answers first;

        $DB->update_record('phraseanalyzer_attempt', array('id' => $this->attemptId, 'answers' => ''));

        $data = array();
        $data['id'] = $this->attemptId;
        $data['answers'] = str_replace('’', '\'', $answers);
//        $data['answers'] = str_replace('\n', ' ', $answers);
        $data['phraseanalyzerid'] = $this->phraseId;
        $data['timemodified'] = time();
        //Only set completed if submitted button pressed
        if ($submit == 1) {
            $data['completed'] = time();
        } else {
            $data['completed'] = 0;
        }
        $DB->update_record('phraseanalyzer_attempt', $data);

        if ($submit == 1) {
            \phraseanalyzer_update_users_grade($this->cm, array($USER->id));
        }
        return true;
    }

    public function delete($term) {
        global $DB;
        $sql = 'SELECT * FROM {phraseanalyzer_term} WHERE cmid = ' . $this->cmid . ' AND termphrase = "' . $term . '"';
        $termRecord = $DB->get_record_sql($sql);
        $DB->delete_records('phraseanalyzer_term', array('id' => $termRecord->id));
    }

    /**
     * 
     * @global \moodle_database $DB
     */
    public function getTermRows() {
        global $DB;

        $PHRASE = new Phrase($this->cmId, $this->phraseId);
        $numberOfColumns = $PHRASE->getNumberOfColumns();
        $TERMS = new Terms($this->cmId);
        $numberOfTerms = $TERMS->getTermCount();
        $totalAnswers = ($numberOfTerms * $numberOfColumns);
        $penalty = $this->penalty;

        $html = '';
        $attempts = $this->attempt;

        $answers = json_decode($attempts->answers);
        $totalCorrect = 0;

        for ($i = 0; $i < count($answers); $i++) {
            //Get answers
            $correctAnswers[$i] = $this->getAnswersComparsion($answers[$i][0], $answers[$i][1], $answers[$i][2], $answers[$i][3], $answers[$i][4]);
            //Set variables
            $td1style = '';
            $answer1style = '';
            $answer2style = '';
            $answer3style = '';
            $answer4style = '';
//            calculate totals. This will be used when attempt has been submitted.
            if ($correctAnswers[$i]['term'] == true) {
                $totalCorrect++;
                if ($this->isAttemptCompleted() == true) {
                    $td1style = 'style="background-color: #85e085;"';
                }
            } else {
                $totalCorrect = $totalCorrect - $penalty;
                if ($this->isAttemptCompleted() == true) {
                    $td1style = 'style="background-color: #ff6666"';
                } else {
                    $td1style = '';
                }
            }
            if ($correctAnswers[$i]['answer1'] == true) {
                $totalCorrect++;
                if ($this->isAttemptCompleted() == true) {
                    $answer1style = 'style="border-color: #85e085;"';
                }
            } else {
                if ($this->isAttemptCompleted() == true) {
                    $answer1style = 'style="border-color: #ff6666"';
                } else {
                    $answer1style = '';
                }
            }
            if ($correctAnswers[$i]['answer2'] == true) {
                $totalCorrect++;
                if ($this->isAttemptCompleted() == true) {
                    $answer2style = 'style="border-color: #85e085;"';
                }
            } else {
                if ($this->isAttemptCompleted() == true) {
                    $answer2style = 'style="border-color: #ff6666"';
                } else {
                    $answer2style = '';
                }
            }
            if ($correctAnswers[$i]['answer3'] == true) {
                $totalCorrect++;
                if ($this->isAttemptCompleted() == true) {
                    $answer3style = 'style="border-color: #85e085;"';
                }
            } else {
                if ($this->isAttemptCompleted() == true) {
                    $answer3style = 'style="border-color: #ff6666"';
                } else {
                    $answer3style = '';
                }
            }
            if ($correctAnswers[$i]['answer4'] == true) {
                $totalCorrect++;
                if ($this->isAttemptCompleted() == true) {
                    $answer4tyle = 'style="border-color: #85e085;"';
                }
            } else {
                if ($this->isAttemptCompleted() == true) {
                    $answer4style = 'style="border-color: #ff6666"';
                } else {
                    $answer4style = '';
                }
            }

            if ($this->isAttemptCompleted() == false) {
                $deleteIcon = '<a href="#" class="deleteRow pull-right"  title="' . $answers[$i][0] . '" ><i class="fa fa-trash"></i></a>';
            } else {
                $deleteIcon = '';
            }
            $html .= '<tr>';
            $html .= '<td ' . $td1style . '><input type="hidden" value="' . $answers[$i][0] . '" id="term-' . $i . '" name="term-' . $i . '"/>' . $answers[$i][0] . $deleteIcon . '</td>';
            $html .= '<td>' . $PHRASE->getColumnHtml(2, $i, $answers[$i][2], $answer1style) . '</td>';
            if ($numberOfColumns >= 3) {
                $html .= '<td>' . $PHRASE->getColumnHtml(3, $i, $answers[$i][3], $answer2style) . '</td>';
            }
            if ($numberOfColumns == 4) {
                $html .= '<td>' . $PHRASE->getColumnHtml(4, $i, $answers[$i][4], $answer3style) . '</td>';
            }
            $html .= '</tr>';
        }

        if ($this->isAttemptCompleted() == true) {
            $html .= '<tr>';
            $html .= '<td colspan="' . $numberOfColumns . '"><h3>' . get_string('your_score', 'phraseanalyzer') . ': ' . $totalCorrect . '/' . $totalAnswers . '</h3></td>';
            $html .= '</tr>';
        }

        return $html;
    }

    /**
     * 
     * @global \moodle_database $DB
     */
    public function getGrade() {
        global $DB;
        $PHRASE = new Phrase($this->cmId);
        $numberOfColumns = $PHRASE->getNumberOfColumns();
        $TERMS = new Terms($this->cmId);
        $numberOfTerms = $TERMS->getTermCount();
        $totalAnswers = ($numberOfTerms * $numberOfColumns);

        $html = '';
        $attempts = $this->attempt;

        $answers = json_decode($attempts->answers);
        
        $totalCorrect = 0;
        $grade = new \stdClass();
        if ($this->isAttemptCompleted()) {
            for ($i = 0; $i < count($answers); $i++) {
                //Get answers
                $correctAnswers[$i] = $this->getAnswersComparsion($answers[$i][0], $answers[$i][1], $answers[$i][2], $answers[$i][3]);

//            calculate totals. This will be used when attempt has been submitted.
                
                if ($correctAnswers[$i]['term'] == true) {
                    $totalCorrect++;
                } else {
                    $totalCorrect = $totalCorrect - $this->penalty;
                }

                if ($correctAnswers[$i]['answer1'] == true) {
                    $totalCorrect++;
                }

                if ($correctAnswers[$i]['answer2'] == true) {
                    $totalCorrect++;
                }


                if ($correctAnswers[$i]['answer3'] == true) {
                    $totalCorrect++;
                }

                if ($correctAnswers[$i]['answer4'] == true) {
                    $totalCorrect++;
                }
            }
            if ($totalAnswers != 0) {
                $percentage = (($totalCorrect / $totalAnswers) * 100);
            } else {
                $percentage = 0;
            }
            $grade->total_answers = $totalAnswers;
            $grade->total_score = $totalCorrect;
            $grade->percentage = round($percentage);
        } else {
            $grade->total_answers = get_string('not_submitted', 'phraseanalyzer');
            $grade->total_score = get_string('not_submitted', 'phraseanalyzer');
            $grade->percentage = get_string('not_submitted', 'phraseanalyzer');
        }

        return $grade;
    }

    public function getTermCount() {
        $attempt = $this->attempt;
        $answers = json_decode($attempt->answers);
        $count = count($answers);

        return $count;
    }

    public function isAttemptCompleted() {
        $attempt = $this->attempt;

        if ($attempt->completed == 0) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @param string $term
     * @param string $answer1
     * @param string $answer2
     * @param string $answer3
     * @param string $answer4
     */
    public function getAnswersComparsion($term, $termOffset, $answer1 = '', $answer2 = '', $answer3 = '', $answer4 = '') {

        $TERMS = new Terms($this->cmId);
        //Get terms to compare with
        $terms = $TERMS->getTermsArray();

        foreach ($terms as $t) {
            //If the term is found, check all answers
            if (trim($t['termphraseoffset']) == $termOffset) {
                //Term was found
                $termCorrect = true;
                //Check answer 1
                if ($answer1 != '') {
                    $tAnswer1 = explode('@@', $t['answer1']);
                    if (in_array($answer1, $tAnswer1, true)) {
                        $answer1Correct = true;
                    } else {
                        $answer1Correct = false;
                    }
                } else {
                    $answer1Correct = false;
                }
                //Check answer 2
                if ($answer2 != '') {
                    $tAnswer2 = explode('@@', $t['answer2']);
                    if (in_array($answer2, $tAnswer2, true)) {
                        $answer2Correct = true;
                    } else {
                        $answer2Correct = false;
                    }
                } else {
                    $answer2Correct = false;
                }
                //Check answer 3
                if ($answer3 != '') {
                    $tAnswer3 = explode('@@', $t['answer3']);
                    if (in_array($answer3, $tAnswer3, true)) {
                        $answer3Correct = true;
                    } else {
                        $answer3Correct = false;
                    }
                } else {
                    $answer3Correct = false;
                }
                //Check answer 4
                if ($answer4 != '') {
                    $tAnswer4 = explode('@@', $t['answer4']);
                    if (in_array($answer4, $tAnswer4, true)) {
                        $answer4Correct = true;
                    } else {
                        $answer4Correct = false;
                    }
                } else {
                    $answer4Correct = false;
                }
                //Becasue the term was found, leave the loop.
                break;
            } else {
                $termCorrect = false;
                $answer1Correct = false;
                $answer2Correct = false;
                $answer3Correct = false;
                $answer4Correct = false;
            }
        }

        //Put all answers into an array and return that array
        $answers = array(
            'term' => $termCorrect,
            'answer1' => $answer1Correct,
            'answer2' => $answer2Correct,
            'answer3' => $answer3Correct,
            'answer4' => $answer4Correct,
        );

        return $answers;
    }

    public function deleteAttempt() {
        global $DB;
        $DB->delete_records('phraseanalyzer_attempt', array('id' => $this->attemptId));
    }

}
