<?php

/**
 * *************************************************************************
 * *                       Phrase Analyzer                                **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  Phrase Analyzer                                           **
 * @name        Phrase Analyzer                                           **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca                               **
 * @author      Patrick Thibaudeau                                        **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

namespace mod_phraseanalyzer;

class Base {
    /*     * Coursemodule */

    public $cm;
    /*     * Phrase and phrase id id */
    public $phrase;
    public $phraseId;
    public $phraseAnalyzerId;
    /*     * Terms */
    public $terms;
    /*     * Do terms exist for this module */
    public $termsExists;
    /*     * Activity is available from this date */
    public $fromDate;
    /*     * Activity is available until this date */
    public $endDate;
    /*     * Penalty */
    public $penalty;

    const ROLE_STUDENT = 0;
    const ROLE_TEACHER = 1;

    /**
     * 
     * @global \moodle_database $DB
     * @param type $cmid
     */
    public function __construct($cmid) {
        global $DB;
        
        $this->cm = get_coursemodule_from_id('phraseanalyzer', $cmid, 0, false, MUST_EXIST);

        $this->phraseAnalyzerId = $this->cm->instance;
        $phraseInstance = $DB->get_record('phraseanalyzer', ['id' => $this->cm->instance]);

        if ($phrase = $DB->get_record('phraseanalyzer_phrase', array('phraseanalyzerid' => $this->cm->instance))) {
            $this->phraseId = $phrase->id;
            $this->phrase = $phrase;
        } else {
            $this->phraseId = 0;
            $this->phrase = new \stdClass();
            $this->phrase->id = 0;
        }

        if ($this->phraseId != 0) {
            if ($terms = $DB->get_records('phraseanalyzer_term', array('phraseanalyzerid' => $this->cm->instance))) {
                $this->terms = $terms;
                $this->termsExists = true;
            } else {
                $this->terms = new \stdClass();
                $this->termsExists = false;
            }
        }

        if (isset($phraseInstance->fromdate)) {
            $this->fromDate = $phraseInstance->fromdate;
        } else {
            $this->fromDate = '';
        }

        if (isset($phraseInstance->enddate)) {
            $this->endDate = $phraseInstance->enddate;
        } else {
            $this->endDate = '';
        }
        
        $this->penalty = $phraseInstance->penalty;
    }
    
    public function getPhraseAnalyzerId() {
        return $this->phraseAnalyzerId;
    }

    public function getCm() {
        return $this->cm;
    }

    public function getPhrase() {
        return $this->phrase;
    }

    public function getPhraseId() {
        return $this->phraseId;
    }

    public function getTerms() {
        return $this->terms;
    }

    public function getTermsExists() {
        return $this->termsExists;
    }

    public function getFromDate() {
        return $this->fromDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }
    
    public function getPenalty() {
        return $this->penalty;
    }

    /**
     * Creates the Moodle page header
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \moodle_page $PAGE
     * @global \stdClass $SITE
     * @param string $url Current page url
     * @param string $pagetitle  Page title
     * @param string $pageheading Page heading (Note hard coded to site fullname)
     * @param array $context The page context (SYSTEM, COURSE, MODULE etc)
     * @param \stdClass $cm a record from course_modules table or cm_info from get_fast_modinfo().
     * @return HTML Contains page information and loads all Javascript and CSS
     */
    public function page($url, $pagetitle, $pageheading, $context, $pagelayout = 'standard') {
        global $CFG, $PAGE, $SITE;

        $stringman = get_string_manager();
        $strings = $stringman->load_component_strings('phraseanalyzer', current_language());

        $PAGE->set_url($url);
        $PAGE->set_title($pagetitle);
        $PAGE->set_heading($pageheading);
        $PAGE->set_pagelayout($pagelayout);
        $PAGE->set_context($context);
        $PAGE->set_cm($this->cm);
        $PAGE->requires->jquery();
        $PAGE->requires->jquery_plugin('ui');
        $PAGE->requires->jquery_plugin('ui-css');
        $PAGE->requires->jquery_plugin('phrase', 'mod_phraseanalyzer');
        $PAGE->requires->jquery_plugin('simpleModal', 'mod_phraseanalyzer');
        $PAGE->requires->strings_for_js(array_keys($strings), 'phraseanalyzer');
        self::loadJSDefaults();
    }

    /**
     * This function provides the javascript console.log function to print out php data to the console for debugging.
     * @param string $object
     */
    public function consoleLog($object) {
        $html = '<script>';
        $html .= 'console.log("' . $object . '")';
        $html .= '</script>';

        echo $html;
    }

    /**
     * 
     * @global \stdClass $CFG
     * @param object $context
     * @return type
     */
    public function getEditorOptions($context) {
        global $CFG;
        return array('subdirs' => true, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1, 'context' => $context, 'trusttext' => true);
    }

    public function getFileManagerOptions($context) {
        global $CFG;
        return array('subdirs' => 0, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => 2);
    }

    public function getNavBar($createBtn = false, $id = '', $toolTip = '') {
        global $CFG;

        $context = \context_system::instance();


        $html = '   <div class="card"> ';
        $html .= '      <div class="card-block"> ';
        if ($createBtn == true) {
            $html .= '<span class="pull-right appBar"><a href="#" id="' . $id . '" data-toggle="tooltip" title="' . $toolTip . '"><i class="fa fa-plus-square"></i></a></span>';
        }
        $html .= '<span class="pull-right appBar"><a href="' . $CFG->wwwroot . '/local/webapp/index.php" data-toggle="tooltip" title="' . get_string('dashboard', 'local_webapp') . '"><i class="fa fa-dashboard"></i></a></span>';
        $html .= '      </div>';
        $html .= '  </div>';

        return $html;
    }

    /**
     * Checks to see if the activity is available based on the fromdate and the enddate
     * return boolean
     */
    public function isAvailable() {

        if (time() >= $this->fromDate) {
            if (time() <= $this->endDate || $this->endDate == 0) {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @global \session $USER
     * @param string $jsFunction Name of JS Function to load
     */
    public function loadJSDefaults($jsFunction = '') {
        global $USER;

        $initjs = "$(document).ready(function() {
                   
                    $(function () {
                        $('[data-toggle=\"tooltip\"]').tooltip()
                    });
                       " . $jsFunction . ";
                    });
               });";

//        echo \html_writer::script($initjs);
    }

}
