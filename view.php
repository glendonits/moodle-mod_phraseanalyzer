<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of phraseanalyzer
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_phraseanalyzer
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Replace phraseanalyzer with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n = optional_param('n', 0, PARAM_INT);  // ... phraseanalyzer instance ID - it should be named as the first character of the module.

if ($id) {
    $cm = get_coursemodule_from_id('phraseanalyzer', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $phraseanalyzer = $DB->get_record('phraseanalyzer', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $phraseanalyzer = $DB->get_record('phraseanalyzer', array('id' => $n), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $phraseanalyzer->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('phraseanalyzer', $phraseanalyzer->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_phraseanalyzer\event\course_module_viewed::create(array(
            'objectid' => $PAGE->cm->instance,
            'context' => $PAGE->context,
        ));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $phraseanalyzer);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/phraseanalyzer/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($phraseanalyzer->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
if (has_capability('mod/phraseanalyzer:addinstance', $PAGE->context)) {
    $PAGE->requires->jquery_plugin('gradeSummary', 'mod_phraseanalyzer');
} else {
    $PAGE->requires->jquery_plugin('attempt', 'mod_phraseanalyzer');
    $PAGE->requires->jquery_plugin('simpleModal', 'mod_phraseanalyzer');
}

//Load strings for js
$stringman = get_string_manager();
$strings = $stringman->load_component_strings('phraseanalyzer', current_language());
$PAGE->requires->strings_for_js(array_keys($strings), 'phraseanalyzer');

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($phraseanalyzer->intro) {
    echo $OUTPUT->box(format_module_intro('phraseanalyzer', $phraseanalyzer, $cm->id), 'generalbox mod_introbox', 'phraseanalyzerintro');
}

if (has_capability('mod/phraseanalyzer:addinstance', $PAGE->context)) {
    //Load objects
    $context = context_module::instance($cm->id);
    $BASE = new \mod_phraseanalyzer\Base($cm->id);
    $GRADES = new \mod_phraseanalyzer\Grades($context, $cm, $course);
       
    if ($BASE->getPhraseId() == 0) {
        $disabled = 'style="pointer-events: none;cursor: pointer;"';
        $step1Completed = 'btn-primary';
        $step2BgColor = 'btn-default';
    } else {
        $disabled = '';
        $step1Completed = 'btn-success';
        if ($BASE->getTermsExists() == false) {
            $step2BgColor = 'btn-primary';
        } else {
            $step2BgColor = 'btn-success';
        }
    }
    
    $initjs = "$(document).ready(function() {
                    gradeSummary();
                });";
    echo html_writer::script($initjs);
    ?>
    <div class="col-xs-12 p-a-1">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title"><?php echo get_string('editing_phases', 'phraseanalyzer');
    echo $OUTPUT->help_icon('editing_phases', 'phraseanalyzer')
    ?></h4>
                <a href="<?php echo $CFG->wwwroot; ?>/mod/phraseanalyzer/edit/step1.php?cmid=<?php echo $cm->id; ?>&id=<?php echo $BASE->getPhraseId(); ?>" class="btn <?php echo $step1Completed; ?>" ><?php echo get_string('step1', 'phraseanalyzer'); ?></a>
                <a href="<?php echo $CFG->wwwroot; ?>/mod/phraseanalyzer/edit/step2.php?cmid=<?php echo $cm->id; ?>&phraseid=<?php echo $BASE->getPhraseId(); ?>" <?php echo $disabled; ?> class="btn <?php echo $step2BgColor; ?>" ><?php echo get_string('step2', 'phraseanalyzer'); ?></a>
                <!--<a href="<?php echo $CFG->wwwroot; ?>/mod/phraseanalyzer/attempt/attempt.php?cmid=<?php echo $cm->id; ?>&phraseid=<?php echo $BASE->getPhraseId(); ?>&role=1" <?php echo $disabled; ?> class="btn <?php echo $step2BgColor; ?>" ><?php echo get_string('preview', 'phraseanalyzer'); ?></a>-->
            </div>
        </div>
    </div>
    <div class="col-xs-12 p-a-1">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title"><?php echo get_string('grading_summary', 'phraseanalyzer');
                ?></h4>
                <div id="SummaryTable">
                    <?php echo $GRADES->get_front_page_submission_view();?>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    $role = \mod_phraseanalyzer\Base::ROLE_STUDENT;
    $initjs = "$(document).ready(function() {
                    attemptAnswers($cm->id);
                });";
    echo html_writer::script($initjs);
    include_once($CFG->dirroot . '/mod/phraseanalyzer/attempt/attempt.php');
}
// Finish the page.
echo $OUTPUT->footer();
