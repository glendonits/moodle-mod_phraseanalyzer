<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the phraseanalyzer module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 *
 * @package    mod_phraseanalyzer
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Execute phraseanalyzer upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_phraseanalyzer_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager(); // Loads ddl manager and xmldb classes.

    if ($oldversion < 2018012500) {

        // Define table phraseanalayzer_phrase to be renamed to NEWNAMEGOESHERE.
        $table = new xmldb_table('phraseanalayzer_phrase');

        // Launch rename table for phraseanalayzer_phrase.
        $dbman->rename_table($table, 'phraseanalyzer_phrase');

        // Define table phraseanalayzer_terms to be renamed to NEWNAMEGOESHERE.
        $table = new xmldb_table('phraseanalayzer_terms');

        // Launch rename table for phraseanalayzer_terms.
        $dbman->rename_table($table, 'phraseanalyzer_terms');

        // Define table phraseanalayzer_attempt to be renamed to NEWNAMEGOESHERE.
        $table = new xmldb_table('phraseanalayzer_attempt');

        // Launch rename table for phraseanalayzer_attempt.
        $dbman->rename_table($table, 'phraseanalyzer_attempt');

        // Phraseanalyzer savepoint reached.
        upgrade_mod_savepoint(true, 2018012500, 'phraseanalyzer');
    }

    if ($oldversion < 2018012501) {



        // Define table phraseanalayzer_terms to be renamed to NEWNAMEGOESHERE.
        $table = new xmldb_table('phraseanalyzer_terms');

        // Launch rename table for phraseanalayzer_terms.
        $dbman->rename_table($table, 'phraseanalyzer_term');


        // Phraseanalyzer savepoint reached.
        upgrade_mod_savepoint(true, 2018012501, 'phraseanalyzer');
    }

    if ($oldversion < 2018020802) {

        // Define field termphraseoffset to be added to phraseanalyzer_term.
        $table = new xmldb_table('phraseanalyzer_term');
        $field = new xmldb_field('termphraseoffset', XMLDB_TYPE_TEXT, null, null, null, null, null, 'termphrase');

        // Conditionally launch add field termphraseoffset.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Phraseanalyzer savepoint reached.
        upgrade_mod_savepoint(true, 2018020802, 'phraseanalyzer');
    }

    if ($oldversion < 2018062800) {

        // Define field penalty to be added to phraseanalyzer.
        $table = new xmldb_table('phraseanalyzer');
        $field = new xmldb_field('penalty', XMLDB_TYPE_NUMBER, '6, 2', null, null, null, '0.5', 'grade');

        // Conditionally launch add field penalty.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Phraseanalyzer savepoint reached.
        upgrade_mod_savepoint(true, 2018062800, 'phraseanalyzer');
    }
    return true;
}
