<?php

/**
 * *************************************************************************
 * *                              webapp                                  **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  webapp                                                    **
 * @name        webapp                                                    **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca                               **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once('config.php');

$action = required_param('action', PARAM_TEXT);

global $USER;

switch ($action) {
    case 'saveAnswers':
        $cmid = required_param('cmid', PARAM_INT);
        $term = trim(required_param('term', PARAM_TEXT));
        $termoffset = trim(required_param('termoffset', PARAM_TEXT));
        $answer1 = optional_param('answer1', '', PARAM_RAW);
        $answer2 = optional_param('answer2', '', PARAM_RAW);
        $answer3 = optional_param('answer3', '', PARAM_RAW);

        $TERM = new \mod_phraseanalyzer\Term($cmid);
        if ($term != 'undefined') {
            $TERM->save($term, $termoffset, $answer1, $answer2, $answer3);
        }
        break;
    case 'deleteAnswer':
        $cmid = required_param('cmid', PARAM_INT);
        $term = trim(required_param('term', PARAM_TEXT));

        $TERM = new \mod_phraseanalyzer\Term($cmid);
        if ($term != 'undefined') {
            $TERM->delete($term);
        }
        break;
    case 'getColumnHtml':
        $cmid = required_param('cmid', PARAM_INT);
        $termCount = required_param('termcount', PARAM_INT);
        $columnNumber = required_param('column', PARAM_INT);
        $cm = get_coursemodule_from_id('phraseanalyzer', $cmid, 0, false, MUST_EXIST);
        
        $PHRASE = new \mod_phraseanalyzer\Phrase($cmid, $cm->instance);
        echo $PHRASE->getColumnHtml($columnNumber, $termCount);
        break;
    case 'saveAttempt':
        global $USER;
        $cmid = required_param('cmid', PARAM_INT);
        $answers = required_param('answers', PARAM_RAW);
        $submit = required_param('submit', PARAM_INT);
        
        $ATTEMPT = new \mod_phraseanalyzer\Attempt($cmid);
        echo $ATTEMPT->save($answers, $submit);
        break;
    case 'deleteAttempt':
        global $USER;
        $cmId = required_param('cmid', PARAM_INT);
        $userId = required_param('userid', PARAM_INT);
        
        $ATTEMPT = new \mod_phraseanalyzer\Attempt($cmId, $userId);
        echo $ATTEMPT->deleteAttempt();
        break;
}

