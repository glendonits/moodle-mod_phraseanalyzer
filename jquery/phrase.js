function init_phraseAnswers(cmid) {
    $('#termSaved').hide();

    activateDeleteRows(cmid);
    $('#paSaveAnswers').click(function () {
        saveAnswers(cmid);
    });
    
    getTerms(cmid);
}

function activateDeleteRows(cmid) {
    var wwwroot = M.cfg.wwwroot;
    $("#termAnswerTable .deleteRow").on("click", function () {
        var tr = $(this).closest('tr');
        var term = $(this).attr('title');

        var confirmation = confirm(M.util.get_string('delete_confirmation', 'phraseanalyzer'));

        if (confirmation == true) {
            $.ajax({
                method: "GET",
                url: wwwroot + "/mod/phraseanalyzer/ajax.php?action=deleteAnswer",
                data: '&cmid=' + cmid
                        + '&term=' + term,
                dataType: "html",
                success: function (html) {

                }
            });

            tr.css("background-color", "#FF3700");
            tr.fadeOut(400, function () {
                tr.remove();
            });
        }
        return false;
    });
}

/**
 * This function is called on the double click of a word
 * @param cmid Integer value
 */
function createTerm(cmid, manual)
{
    var txt = '';
    var termCount = $('#termCount').val();
    var wwwroot = M.cfg.wwwroot;
    var numberOfColumns = $('#numberOfColumns').val();
    //Get the text
    if (window.getSelection)
    {
        txt = window.getSelection();
    } else if (document.getSelection)
    {
        txt = document.getSelection();
    } else if (document.selection)
    {
        txt = document.selection.createRange().text;
    } else {
        return false;
    }

    if (txt != '') {
        termCount++;
        var offsets = getTermOffsets(txt);
        anchorOffset = offsets.anchorOffset;
        focusOffset = offsets.focusOffset;
        selectedText = offsets.selectedText;

        var row = '<tr>'
                + '<td><input type="hidden" id="termOffset' + termCount + '" value="' + anchorOffset  + ';' + selectedText.trim() + ';' + focusOffset + '"><div class="col-md-11"><textarea class="form-control"  id="term' + termCount + '" name="term' + termCount + '">' + txt + '</textarea></div><div class="col-md-1"><a href="#" class="deleteRow" title="' + anchorOffset + selectedText.trim() + focusOffset + '" ><i class="fa fa-trash"></i></a></div></td>'
                + '<td><textarea class="form-control" id="answer1-' + termCount + '" name="answer1-' + termCount + '"></textarea></td>';
        if (numberOfColumns >= 3) {
            row += '<td><textarea class="form-control" id="answer2-' + termCount + '" name="answer2-' + termCount + '"></textarea></td>';
        }
        if (numberOfColumns == 4) {
            row += '<td><textarea class="form-control" id="answer3-' + termCount + '" name="answer3-' + termCount + '"></textarea></td>';
        }

        $('#termAnswerTable > tbody:last-child').append(row);

        $('#termCount').val(termCount);

        activateDeleteRows(cmid);

        return false;
    }
}

function saveAnswers(cmid) {
    var wwwroot = M.cfg.wwwroot;
    var termCount = $('#termCount').val();
    var err = M.util.get_string('no_answers_available', 'phraseanalyzer');



    if (termCount == 0) {
        alert(err);
    } else {
        for (i = 0; i < termCount + 1; i++) {
            var answer1 = $('#answer1-' + i).val();
            var answer2 = $('#answer2-' + i).val();
            var answer3 = $('#answer3-' + i).val();

            if (answer1 != undefined) {
                answer1 = answer1.replace(/(?:\r\n|\r|\n)/g, '@@');
            } else {
                answer1 = '';
            }
            if (answer2 != undefined) {
                answer2 = answer2.replace(/(?:\r\n|\r|\n)/g, '@@');
            } else {
                answer2 = '';
            }
            if (answer3 != undefined) {
                answer3 = answer3.replace(/(?:\r\n|\r|\n)/g, '@@');
            } else {
                answer3 = '';
            }

            $.ajax({
                method: "GET",
                url: wwwroot + "/mod/phraseanalyzer/ajax.php?action=saveAnswers",
                data: '&cmid=' + cmid
                        + '&term=' + $('#term' + i).val()
                        + '&termoffset=' + $('#termOffset' + i).val()
                        + '&answer1=' + answer1
                        + '&answer2=' + answer2
                        + '&answer3=' + answer3,
                dataType: "html",
                success: function () {
                    var message = $('#termSaved').show();
                    message.fadeOut(2400, function () {
                        message.hide();
                    });
                }
            });
        }
    }
}

function getTermOffsets(selection) {
    //Get the actual full text
    var fullText = selection.anchorNode.data;
    //Get the actual selected term based on achorOffset (start position) and focusOffset (end position)
    var selectedText = fullText.substring(selection.anchorOffset, selection.focusOffset);
    //Get the texts length
    var termLength = selectedText.length;
    //Get new text lenght when removing whitespace at the end of the term
    var newFocusOffsetTermLength = selectedText.replace(/\s+$/gm, '').length;
    //calculate the length difference
    var focusLengthDifference = termLength - newFocusOffsetTermLength;
    //Set new focusOffset value
    var focusOffset = selection.focusOffset - focusLengthDifference;
    //Get the new text lenght when removing whitspace from the start
    var newAnchorOffsetTermLength = selectedText.replace(/^\s+/gm, '').length;
    //calculate the length difference
    var anchorLengthDifference = termLength - newAnchorOffsetTermLength;
    //Set new focusOffset value
    var anchorOffset = selection.anchorOffset + anchorLengthDifference;

    var offsets = {
        'anchorOffset': anchorOffset,
        'focusOffset': focusOffset,
        'selectedText': selectedText.trim()
    }

    return offsets;
}
