$(document).ready(function () {
    $('#participantSubmission').DataTable({
        stateSave: true
    });
});

function deleteAttempt(cmId, userId) {
    var wwwroot = M.cfg.wwwroot;
    var message = M.util.get_string('delete_confirmation', 'phraseanalyzer');
    var deleteAttempt = confirm(message);

    if (deleteAttempt) {
        $.ajax({
            method: "GET",
            url: wwwroot + "/mod/phraseanalyzer/ajax.php?action=deleteAttempt",
            data: '&cmid=' + cmId + '&userid=' + userId,
            dataType: "html",
            success: function () {
                location.reload();
            }
        });
    }
}
