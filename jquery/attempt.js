function attemptAnswers(cmid) {
    //Hide Save message
    $('#termSaved').hide();
    $('#submitDialog').hide();

    //Make deleting rows available
    asActivateDeleteRows(cmid);

    //Save when user click save answers button
    $('#paSaveAnswers').click(function () {
        asSaveAnswers(cmid, 0);
    });

    $('#paSubmitAnswers').click(function () {
        asSubmitAnswers(cmid);
    });
}

function asActivateDeleteRows(cmid) {
    //Get moodle base URL
    var wwwroot = M.cfg.wwwroot;
    var termCount = $('#termCount').val();
    parseInt(termCount);
    //Delete button clicked
    $("#asTermAnswerTable .deleteRow").on("click", function () {
        var tr = $(this).closest('tr');
        var term = $(this).attr('title');
        //Confirmation message
        var confirmation = confirm(M.util.get_string('delete_confirmation', 'phraseanalyzer'));

        if (confirmation == true) {
            tr.css("background-color", "#FF3700");
            tr.fadeOut(400, function () {
                tr.remove();
            });
            termCount = termCount - 1;
            if(isNaN(termCount)) {
                termCount = 0;
            }
            $('#termCount').val(termCount);
        }

        return false;
    });
}

/**
 * This function is called on the double click of a word
 * @param cmid Integer value
 */
function asCreateTerm(cmid)
{
    var txt = '';
    var termCount = $('#termCount').val();
    var wwwroot = M.cfg.wwwroot;
    var numberOfColumns = $('#numberOfColumns').val();
    parseInt(termCount);
    //Get the text
    if (window.getSelection)
    {
        txt = window.getSelection();
    } else if (document.getSelection)
    {
        txt = document.getSelection();
    } else if (document.selection)
    {
        txt = document.selection.createRange().text;
    } else {
        return false;
    }

    if (txt != '') {
        var offsets = getTermOffsets(txt);
        anchorOffset = offsets.anchorOffset;
        focusOffset = offsets.focusOffset;
        selectedText = offsets.selectedText;
        
        var row = '<tr>'
                + '<td><input type="hidden" value="' + anchorOffset + ';' + selectedText + ';' + focusOffset + '" id="termOffset-' + termCount + '" name="termoffset-' + termCount + '"/>' + '<input type="hidden" value="' + selectedText + '" id="term-' + termCount + '" name="term-' + termCount + '"/>' + selectedText.trim() + '<a href="#" class="deleteRow pull-right" title="' + txt + '" ><i class="fa fa-trash"></i></a></td>'
                + '<td>' + getColumnHtml(cmid, 2, termCount) + '</td>';
        if (numberOfColumns >= 3) {
            row += '<td>' + getColumnHtml(cmid, 3, termCount) + '</td>';
        }
        if (numberOfColumns == 4) {
            row += '<td>' + getColumnHtml(cmid, 4, termCount) + '</td>';
        }

        $('#asTermAnswerTable > tbody:last-child').append(row);

        termCount++;
        $('#termCount').val(termCount);

        asActivateDeleteRows(cmid);

        asSaveAnswers(cmid, 0);

        return false;
    }
}

function asSaveAnswers(cmid, submit) {
    var wwwroot = M.cfg.wwwroot;
    var termCount = $('#termCount').val();
    var numberOfColumns = $('#numberOfColumns').val();
    var err = M.util.get_string('no_answers_available', 'phraseanalyzer');


    if (termCount == 0) {
        alert(err);
    } else {
        var answers = new Array();
        for (i = 0; i < termCount; i++) {

            var term = $.trim($('#term-' + i).val());
            var termOffset = $.trim($('#termOffset-' + i).val());
            var answer1 = $.trim($('#answer2-' + i).val());
            var answer2 = $.trim($('#answer3-' + i).val());
            var answer3 = $.trim($('#answer4-' + i).val());
            answers[i] = new Array(
                    term,
                    termOffset,
                    answer1,
                    answer2,
                    answer3
                    );
        }

        $.ajax({
            method: "GET",
            url: wwwroot + "/mod/phraseanalyzer/ajax.php?action=saveAttempt",
            data: '&cmid=' + cmid + '&submit=' + submit
                    + '&answers=' + JSON.stringify(answers),
            dataType: "html",
            success: function () {
                if (submit == 1) {
                    location.reload();
                } else {
                    var message = $('#termSaved').show();
                    message.fadeOut(2400, function () {
                        message.hide();
                    });
                }
            }
        });
    }
}

function asSubmitAnswers(cmid) {
    var cancel = M.util.get_string('cancel', 'phraseanalyzer');
    var submit = M.util.get_string('submit_answers', 'phraseanalyzer');
    var confirmSubmit = confirm(submit);

    if (confirmSubmit) {
        asSaveAnswers(cmid, 1);
    }
//    $('#submitDialog').dialog({
//        resizable: false,
//        height: "auto",
//        width: 400,
//        modal: true,
//        buttons: [{
//                text: submit,
//                "id": "btnSubmit",
//                click: function () {
//                    asSaveAnswers(cmid, 1);
//                },
//
//            }, {
//                text: cancel,
//                click: function () {
//                    $(this).dialog("close");
//                },
//            }],
//    });


}

/**
 * Get column HTML as per column configuration in phrase analyzer setup
 * @param int cmid
 * @param int columnNumber
 * @param int termCount
 * @returns html
 */
function getColumnHtml(cmid, columnNumber, termCount) {
    var wwwroot = M.cfg.wwwroot;
    $.ajax({
        method: "GET",
        url: wwwroot + "/mod/phraseanalyzer/ajax.php?action=getColumnHtml",
        data: '&cmid=' + cmid + '&column=' + columnNumber + '&termcount=' + termCount,
        dataType: "html",
        async: false,
        success: function (html) {
            result = html;
        }
    });

    return result;
} 

function getTermOffsets(selection) {
    //Get the actual full text
    var fullText = selection.anchorNode.data;
    //Get the actual selected term based on achorOffset (start position) and focusOffset (end position)
    var selectedText = fullText.substring(selection.anchorOffset, selection.focusOffset);
    //Get the texts length
    var termLength = selectedText.length;
    //Get new text lenght when removing whitespace at the end of the term
    var newFocusOffsetTermLength = selectedText.replace(/\s+$/gm, '').length;
    //calculate the length difference
    var focusLengthDifference = termLength - newFocusOffsetTermLength;
    //Set new focusOffset value
    var focusOffset = selection.focusOffset - focusLengthDifference;
    //Get the new text lenght when removing whitspace from the start
    var newAnchorOffsetTermLength = selectedText.replace(/^\s+/gm, '').length;
    //calculate the length difference
    var anchorLengthDifference = termLength - newAnchorOffsetTermLength;
    //Set new focusOffset value
    var anchorOffset = selection.anchorOffset + anchorLengthDifference;

    var offsets = {
        'anchorOffset': anchorOffset,
        'focusOffset': focusOffset,
        'selectedText': selectedText.trim()
    }

    return offsets;
}