<?php

/**
 * *************************************************************************
 * *                              webapp                                  **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  webapp                                                    **
 * @name        webapp                                                    **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca                               **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
$plugins = array(
    'phrase' => array('files' => array(
            'phrase.js',
            'attempt.js'
        )),
    'attempt' => array('files' => array(
            'attempt.js'
        )),
    'attemptview' => array('files' => array(
            'attemptview.css',
            'attemptview.js'
        )),
    'gradeSummary' => array('files' => array(
            'datatables/datatables.min.css',
            'datatables/datatables.min.js',
            'gradeSummary.js'
        )),
    'simpleModal' => array('files' => array(
            'simpleModal/simpleModal.min.css',
            'simpleModal/simpleModal.min.js'
        )),
);
