<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for phraseanalyzer
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_phraseanalyzer
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
$string["action"] = "Action";
$string["add_term"] = "Add";
$string["attempt"] = "Attempt";
$string["availability"] = "Availability";
$string["available_answers"] = "Possible answers";
$string['cancel'] = "Cancel";
$string["column1"] = "Column 1";
$string["column2"] = "Column 2";
$string["column3"] = "Column 3";
$string["column4"] = "Column 4";
$string["column_options"] = "Column options";
$string["columns"] = "Column options";
$string["data_saved"] = "The changes have been saved";
$string["delete_attempt"] = "Delete attempt";
$string["delete_confirmation"] = "Are you sure you want to delete this record. This cannot be undone.";
$string['editing'] = 'Text editing';
$string['editing_phases'] = 'Editing phases';
$string['editing_phases_help'] = 'Use the editing phases below to setup your assignment.'
        . '<ol>'
        . '<li><b>Text and columns:</b> Setup instructions and the text to be used by students. You also setup your column options here.</li>'
        . '<li><b>Terms and answers:</b> Prepare your terms and answers based on the text entered in the Text and columns phase.</li>'
        . '<li><b>Preview:</b> Preview the assignment.</li>'
        . '</ol>';
$string['end_date'] = 'Due date';
$string['from_date'] = 'Allow attempts from';
$string['grade'] = 'Grade';
$string['grading_summary'] = 'Grading summary';
$string['instructions'] = 'Instructions';
$string['modulename'] = 'Grammatical text parser';
$string['modulenameplural'] = 'Grammatical text parser';
$string['modulename_help'] = 'Use the Grammatical text parser module for... | The Grammatical text parser module allows...';
$string['name'] = 'Name';
$string['no_answers_available'] = 'You have no terms to save.';
$string['not_submitted'] = 'No attempt submitted.';
$string["penalty"] = "Penalty";
$string["penalty_help"] = "This is the number of points that will be subtracted for each missed terms or extra terms on an attempt.";
$string["phrase"] = "Text";
$string['phraseanalyzer:addinstance'] = 'Add a new text analyzer';
$string['phraseanalyzer:submit'] = 'Submit text analyzer';
$string['phraseanalyzer:view'] = 'View text analyzer';
$string['phraseanalyzerfieldset'] = 'Custom example fieldset';
$string['phraseanalyzername'] = 'Grammatical text parser name';
$string['phraseanalyzername_help'] = '';
$string['phraseanalyzer'] = 'Grammatical text parser';
$string['pluginadministration'] = 'Grammatical text administration';
$string['pluginname'] = 'Grammatical text parser';
$string['preview'] = 'Preview';
$string["questiontext"] = "Question";
$string["required"] = "This field is required";
$string["save_answers"] = "Save answers";
$string["select"] = "Select";
$string["step1"] = "Text and columns";
$string["step2"] = "Terms and answers";
$string["step2_instructions"] = "Double click on the values to add them to the terms table. You will then be able to add answers for each terms";
$string["submit_answers"] = "Submit answers";
$string["submit_answers_dialog"] = "Are you sure you want to submit your answers. Once submitted, you may or may not be able to retry your attempt.";
$string["the_answers"] = "The answers";
$string["the_answers_help"] = "In each available text area, enter each possible answer for each individual columns. Enter each answer on a new line.";
$string["the_phrase"] = "The text";
$string["the_phrase_help"] = "Select a word or sentence by clicking and dragging your mouse within the text below. When you remove your finger for the mouse, the"
        . " sentence or word will be added to the table below.";
$string["the_question"] = "The question";
$string["use_column_options"] = "Create options for this column";
$string["use_column_options_help"] = "Selecting yes will enable the field below. Enter each value on a seperate line withn the field. "
        . "These values will be converted into a select menu when doing the exercise";
$string['view_attempt'] = 'View attempt';
$string['your_score'] = 'Your score';


/* *****************************
 * ********* PRIVACY ***********
 * *****************************
 */
$string['privacy:metadata:phraseanalyzer_attempt'] = 'Information about the attempt. This includes the answers given to by the user, the status of completion and the time the attempt was started, modified and completed';
$string['privacy:metadata:userid'] = 'The ID of the user has has done the attempt';
$string['privacy:metadata:phraseanalyzerid'] = 'The ID of the activity in order to reference the paragraph.';
$string['privacy:metadata:answers'] = 'The answers given by the user.';
$string['privacy:metadata:attempt'] = 'The attempt done by the user. Currently only one attempt is possible.';
$string['privacy:metadata:timecreated'] = 'The time the attempt was started by the user.';
$string['privacy:metadata:timemodified'] = 'The last time the attempt was modified by the user.';
$string['privacy:metadata:completed'] = 'The time that the user submitted the activity.';