<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for phraseanalyzer
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_phraseanalyzer
 * @copyright  2018 Patrick Thibaudeau Glendon Yorku University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
$string["action"] = "Action";
$string["add_term"] = "Ajouter";
$string["attempt"] = "Tentative";
$string["available_answers"] = "Réponses possible";
$string["cancel"] = "Annuler";
$string["column1"] = "Colonne 1";
$string["column2"] = "Colonne 2";
$string["column3"] = "Colonne 3";
$string["column4"] = "Colonne 4";
$string["column_options"] = "Options de colonne";
$string["columns"] = "Column options";
$string["data_saved"] = "Les changements ont été enregistrés";
$string["delete_confirmation"] = "Êtes-vous sûr de vouloir supprimer ce document. Ça ne peut pas être annulé.";
$string["delete_attempt"] = "Supprimer la tentative";
$string['editing'] = 'Édition de texte';
$string['editing_phases'] = 'Les phase pour l\'édition';
$string['editing_phases_help'] = 'Utilisez les phases d\'édition ci-dessous pour configurer votre affectation.'
        . '<ol>'
        . '<li><b>Texte et colonnes: </b> Vous ajoutez les instructions d\'utilisation et le texte dans cette phase. Vous devez également configuré vos options de colonne ici.</li>'
        . '<li><b>Termes et réponses: </b> Préparez vos termes et réponses en fonction du texte saisi dans la phase Texte et colonnes.</li>'
        . '<li><b>Aperçu: </b>Aperçu de l\'affectation.</li>'
        . '</ol>';
$string['grade'] = 'Note';
$string['grading_summary'] = 'Résumé de l\'évaluation';
$string['instructions'] = 'Instructions';
$string['modulename'] = 'Analyseur grammatical de texte';
$string['modulenameplural'] = 'Analyseur grammatical de texte';
$string['modulename_help'] = '';
$string['name'] = 'Nom';
$string['no_answers_available'] = 'Vous n\'avez pas de terme à sauvegarder.';
$string['not_submitted'] = 'Aucune tentative soumise';
$string["phrase"] = "Texte";
$string['phraseanalyzer:addinstance'] = 'Ajouter un nouveau Analyseur grammatical de texte';
$string['phraseanalyzer:submit'] = 'Envoyer un analyseur grammatical de texte';
$string['phraseanalyzer:view'] = 'Voir l\'analyseur grammatical de texte';
$string['phraseanalyzername'] = 'Nom de l\'analyseur grammatical de texte';
$string['phraseanalyzername_help'] = 'This is the content of the help tooltip associated with the phrase analyzername field. Markdown syntax is supported.';
$string['phraseanalyzer'] = 'Analyseur grammatical de texte';
$string['pluginadministration'] = 'Administration de l\'analyseur grammatical de texte';
$string['pluginname'] = 'Analyseur grammatical de texte';
$string['preview'] = 'Aperçu';
$string["questiontext"] = "Question";
$string["required"] = "Ce champ est requis";
$string["save_answers"] = "Enregistrer les réponses";
$string["select"] = "Sélectionner";
$string["step1"] = "Texte et colonnes";
$string["step2"] = "Termes et réponses";
$string["step2_instructions"] = "Double-cliquez sur les valeurs pour les ajouter au tableau des termes. Vous pourrez ensuite ajouter des réponses pour chaque termes.";
$string["submit_answers"] = "Soumettre les réponses";
$string["submit_answers_dialog"] = "Êtes-vous sûr de vouloir soumettre votre réponse? Une fois soumis, vous pouvez ou ne pouvez pas réessayer votre tentative.";
$string["the_answers"] = "Les réponses";
$string["the_answers_help"] = "Dans chaque zone de texte disponible, saisissez chaque réponse possible pour chaque colonne. Entrez chaque réponse sur une nouvelle ligne.";
$string["the_phrase"] = "Le texte";
$string["the_phrase_help"] = "Sélectionnez un mot ou une phrase en cliquant et en faisant glisser votre souris dans le texte ci-dessous. Lorsque vous retirez votre doigt pour la souris,"
        . " La phrase ou le mot sera ajouté au tableau ci-dessous.";
$string["the_question"] = "La question";
$string["use_column_options"] = "Créer des options pour cette colonne";
$string["use_column_options_help"] = "La sélection de oui permettra le champ ci-dessous. Entrez chaque valeur sur une ligne distincte dans le champ. "
        . "Ces valeurs seront converties en menu sélectionné lors de l'exercice";
$string['view_attempt'] = 'Voir la tentative';
$string['your_score'] = 'Votre score';
